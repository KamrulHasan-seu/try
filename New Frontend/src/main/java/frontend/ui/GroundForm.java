package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.converter.StringToDoubleConverter;
import com.vaadin.flow.data.converter.StringToIntegerConverter;
import frontend.model.Ground;
import frontend.service.GroundService;

public class GroundForm extends VerticalLayout {

    private GroundService groundService = new GroundService();

    private TextField id = new TextField("Id");
    private TextField name = new TextField("Name");
    private TextField city = new TextField("City");
    private TextField country = new TextField("Country");
    private TextField longitude = new TextField("Longitude");
    private TextField latitude = new TextField("Latitude");
    private TextField capacity = new TextField("Capacity");
    private DatePicker date = new DatePicker("InaugurationDate");
    private Button save = new Button("Save");
    private Button delete = new Button("Delete");

    private Ground ground;
    private InsertGround mainUI;
    private Binder<Ground> binder = new Binder<>(Ground.class);


    public GroundForm(InsertGround myUI) {
        this.mainUI = myUI;

        //this.setSpacing(false);
        setSizeUndefined();
        HorizontalLayout buttons = new HorizontalLayout(save, delete);
        save.getElement().getThemeList().add("primary");
        add(id, name, city, country, longitude, latitude, capacity, date, buttons);


        binder.forField(id)
                .asRequired()
                .withValidator(s -> s.length() >= 3, "Id Should be 4 digits Long")
                .withValidator(s -> !s.isEmpty() && s != null, "Id Can not be Empty")
                .bind(Ground::getGroundId, Ground::setGroundId);
        binder.forField(name)
                .asRequired()
                .withValidator(s -> s.length() >= 3, "Name Should be 4 Character Long")
                .withValidator(s -> !s.isEmpty() && s != null, "Name Can not be Empty")
                .bind(Ground::getGroundName, Ground::setGroundName);
        binder.forField(city)
                .asRequired()
                .withValidator(s -> s.length() >= 2, "City Should be 2 Character Long")
                .withValidator(s -> !s.isEmpty() && s != null, "Name Can not be Empty")
                .bind(Ground::getCity, Ground::setCity);
        binder.forField(country)
                .asRequired()
                .withValidator(s -> s.length() >= 3, "Country Should be 4 Character Long")
                .withValidator(s -> !s.isEmpty() && s != null, "Country Can not be Empty")
                .bind(Ground::getCountry, Ground::setCountry);

        binder.forField(longitude)
                .asRequired()
                .withConverter(
                        new StringToDoubleConverter("Must enter a Double Number"))
                .bind(Ground::getLongitude, Ground::setLongitude);
        binder.forField(latitude)
                .asRequired()
                .withConverter(
                        new StringToDoubleConverter("Must enter a Double Number"))
                .bind(Ground::getLatitude, Ground::setLatitude);
        binder.forField(capacity)
                .asRequired()
                .withConverter(
                        new StringToIntegerConverter("Must enter a number"))
                .bind(Ground::getCapacity, Ground::setCapacity);
        binder.forField(date)
                .asRequired()
                .bind(Ground::getInaugurationDate, Ground::setInaugurationDate);


        save.addClickListener(e -> this.save());
        delete.addClickListener(e -> this.delete());
        save.setEnabled(false);
        binder.addStatusChangeListener(status -> {
            save.setEnabled(!status.hasValidationErrors());
        });
    }


    public void setGround(Ground ground) {
        this.ground = ground;
        binder.readBean(ground);

        // Show delete button for only customers already in the database
        delete.setVisible(ground.isPersisted());
        setVisible(true);
        id.focus();
        //this.setSpacing(false);
    }

    private void delete() {
        groundService.delete(ground);
        setVisible(false);
        notifyMainView();
    }


    private void save() {
        Ground savedGround = new Ground();
        try {
            binder.writeBean(savedGround);
            if (groundService.getGroundById(savedGround.getGroundId()).isPresent()) {
                groundService.updateGround(savedGround);
                binder.readBean(new Ground());
            } else {
                groundService.saveGround(savedGround);
                binder.readBean(new Ground());
            }
        } catch (ValidationException e) {
            e.printStackTrace();
        }

        notifyMainView();
    }

    private void notifyMainView() {
        if (mainUI != null) {
            Notification.show("Updated");
            mainUI.updateList();
        }

    }
}

