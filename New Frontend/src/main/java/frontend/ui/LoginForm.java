package frontend.ui;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import frontend.security.AccessControl;
import frontend.security.AccessControlFactory;

import javax.annotation.security.RolesAllowed;

@Route(value = "login")
public class LoginForm extends VerticalLayout {
    private com.vaadin.flow.component.login.LoginForm loginForm;
    private AccessControl accessControl;
    public LoginForm() {
        accessControl = AccessControlFactory.getInstance().createAccessControl();
        loginForm = new com.vaadin.flow.component.login.LoginForm();
        add(loginForm);
        loginForm.addLoginListener(this::login);
    }
    private void login(com.vaadin.flow.component.login.LoginForm.LoginEvent event) {
        if (accessControl.signIn(event.getUsername(), event.getPassword())) {
            registerAdminViewIfApplicable();
            getUI().get().navigate("");

        } else {
            event.getSource().setError(true);
        }
    }

    private void registerAdminViewIfApplicable() {
        // register the admin view dynamically only for any admin user logged in
        if (accessControl.isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
            RouteConfiguration.forSessionScope().setRoute("admin",AdminPanel.class);

            // as logout will purge the session route registry, no need to
            // unregister the view on logout
        }
    }

}
