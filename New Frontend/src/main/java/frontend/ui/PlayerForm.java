package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.converter.StringToDoubleConverter;
import com.vaadin.flow.data.converter.StringToIntegerConverter;
import frontend.model.*;
import org.vaadin.gatanaso.MultiselectComboBox;
import frontend.service.PlayerService;


public class PlayerForm extends FormLayout {
    Binder<Player> binder = new Binder<>(Player.class);
    private TextField id = new TextField("Id");
    private ComboBox<Country> playingTeam = new ComboBox<>("Playing Team");
    private TextField fname = new TextField("First Name");
    private TextField mname = new TextField("Middle Name");
    private TextField lname = new TextField("Last Name");
    private TextField nationality = new TextField("Nationality");
    private DatePicker date = new DatePicker("DOB");
    private TextField height = new TextField("Height");
    private TextField jerseyno = new TextField("Jersey No");
    private MultiselectComboBox<PlayerRole> playerRoleComboBox = new MultiselectComboBox<>();
    private ComboBox<BatsmanType> batsmanTypeComboBox = new ComboBox<>("Batsman Type");
    private ComboBox<BowlerType> bowlerTypeComboBox = new ComboBox<>("Bowler Type");
    private Button save = new Button("Save");
    private Button delete = new Button("Delete");

    private Player player = new Player();
    private PlayerService playerService = new PlayerService();
    private InsertPlayer mainUI;

    public PlayerForm(InsertPlayer myui) {
        this.mainUI = myui;


        HorizontalLayout buttons = new HorizontalLayout(save, delete);
        save.getElement().getThemeList().add("primary");
        add(id,playingTeam, fname, mname, lname, nationality, date, height, jerseyno, playerRoleComboBox, batsmanTypeComboBox, bowlerTypeComboBox, buttons);

        playingTeam.setItems(Country.values());
        playerRoleComboBox.setPlaceholder("Select Player Type");
        playerRoleComboBox.setItems(PlayerRole.values());

        batsmanTypeComboBox.setPlaceholder("Select batsman Type");
        batsmanTypeComboBox.setItems(BatsmanType.values());
        bowlerTypeComboBox.setPlaceholder("Select BowlerType");
        bowlerTypeComboBox.setItems(BowlerType.values());


        binder.forField(id)
                .asRequired()
                .withValidator(s -> s.length() >= 2, "Id Should be 2 digits Long")
                .withValidator(s -> !s.isEmpty() && s != null, "Id Can not be Empty")
                .bind(Player::getPlayerId, Player::setPlayerId);
        binder.forField(fname)
                .asRequired()
                .withValidator(s -> s.length() >= 2, "Name Should be 2 Character Long")
                .withValidator(s -> !s.isEmpty() && s != null, "First Name Can not be Empty")
                .bind(Player::getFirstName, Player::setFirstName);
        binder.forField(mname)
                .asRequired()
                .withValidator(s -> s.length() >= 2, "MiddleName Should be 2 Character Long")
                .withValidator(s -> !s.isEmpty() && s != null, "Middle Name Can not be Empty")
                .bind(Player::getMiddleName, Player::setMiddleName);
        binder.forField(lname)
                .asRequired()
                .withValidator(s -> s.length() >= 2, "LastName Should be 2 Character Long")
                .withValidator(s -> !s.isEmpty() && s != null, "Last Name Can not be Empty")
                .bind(Player::getLastName, Player::setLastName);
        binder.forField(nationality)
                .asRequired()
                .withValidator(s -> s.length() >= 2, "Nationality Should be 2 Character Long")
                .withValidator(s -> !s.isEmpty() && s != null, "Nationality Can not be Empty")
                .bind(Player::getNationality, Player::setNationality);
        binder.forField(date)
                .asRequired()
                .bind(Player::getDateOfBirth, Player::setDateOfBirth);
        binder.forField(height)
                .asRequired()
                .withValidator(s -> s.length() >= 2, "Name Should be 2 Character Long")
                .withValidator(s -> !s.isEmpty() && s != null, "Height Can not be Empty")
                .withConverter(
                        new StringToDoubleConverter("Must enter a double number"))
                .bind(Player::getHeight, Player::setHeight);
        binder.forField(jerseyno)
                .asRequired()
                .withValidator(s -> s.length() >= 1, "JerseyNo Should be 1 Character Long")
                .withValidator(s -> !s.isEmpty() && s != null, "Jersey No. Can not be Empty")
                .withConverter(
                        new StringToIntegerConverter("Must enter a Integer number"))
                .bind(Player::getJerseyNumber, Player::setJerseyNumber);
        binder.forField(playerRoleComboBox)
                .asRequired()
                .bind(Player::getPlayerRole,Player::setPlayerRole);
        binder.forField(batsmanTypeComboBox)
                .asRequired()
                .bind(Player::getBatsmanType, Player::setBatsmanType);
        binder.forField(bowlerTypeComboBox)
                .asRequired()
                .bind(Player::getBowlerType, Player::setBowlerType);

        save.addClickListener(e -> this.save());
        delete.addClickListener(e -> this.delete());

        save.setEnabled(false);
        binder.addStatusChangeListener(status -> {
            save.setEnabled(!status.hasValidationErrors());
        });
    }

    public void setPlayer(Player player) {
        this.player = player;
        binder.readBean(player);

        // Show delete button for only customers already in the database
        delete.setVisible(player.isPersisted());
        setVisible(true);
        id.focus();
    }

    private void delete() {
        playerService.delete(player);
        setVisible(false);
        notifyMainView();
    }

    private void save() {
        Player savedPlayer = new Player();
        try {
            binder.writeBean(savedPlayer);
            if (playerService.getPlayerById(savedPlayer.getPlayerId()).isPresent()) {
                playerService.updatePlayer(savedPlayer);
                binder.readBean(new Player());
            } else {
                playerService.savePlayer(savedPlayer);
                binder.readBean(new Player());
            }
        } catch (ValidationException e) {
            e.printStackTrace();
        }
        notifyMainView();
    }

    private void notifyMainView() {
        if (mainUI != null) {
            Notification.show("Updated");
            mainUI.updateList();
        }

    }
}
