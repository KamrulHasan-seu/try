package frontend.ui;

import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import frontend.model.home.Articles;
import frontend.service.ApiService;

@Route(value = "homeView",layout = MainView.class)
public class HomeView extends VerticalLayout {

    private ApiService apiService = new ApiService();

    private VerticalLayout recentTimeLineLayout = new VerticalLayout();
    private VerticalLayout topTimeLineLayout = new VerticalLayout();
    private HorizontalLayout dividerLayout = new HorizontalLayout();
    private Footer footer = new Footer();


    public HomeView() {
        topTimeLineLayout.add(new H2("Latest News"));
        recentTimeLineLayout.add(new H3("Recent News"));
        this.add(dividerLayout, footer);
        dividerLayout.add(recentTimeLineLayout, topTimeLineLayout);
        recentTimeLineLayout.setWidth("40%");

        recentTimeLineLayout.setMargin(false);
        topTimeLineLayout.setMargin(false);
        topTimeLineLayout.setPadding(false);
        dividerLayout.setPadding(false);
        dividerLayout.setMargin(false);

        this.setMargin(false);
        this.setPadding(false);

        liveScores();
        recentHeadLines();
        latestNews();
        footer();
    }

    private void latestNews() {
        for (Articles news : apiService.getAllTopHeadlines()) {
            Label title = new Label(news.getTitle());
            title.getStyle().set("font-weight", "bold");
            title.getStyle().set("font-size", "20px");
            topTimeLineLayout.add(title);

            Image image = new Image();
            if (news.getUrlToImage() != null) {
                image.setSrc(news.getUrlToImage().toString());
            }
            topTimeLineLayout.add(image);
            image.setWidth("100%");

            String descriptionNews = news.getDescription().replace(" | ESPNcricinfo.com", "");
            topTimeLineLayout.add(new Paragraph(descriptionNews));

            Anchor anchor = new Anchor();
            anchor.setText("see more...");
            anchor.getElement().addEventListener("click", e -> anchor.setHref(news.getUrl().toString()));
            anchor.getStyle().set("margin","0px");
            topTimeLineLayout.add(anchor);

            topTimeLineLayout.add(new Hr());
        }
    }

    private void footer() {
        Div div = new Div(new Label("News Credit goes to EspnCrickInfo"));
        div.getStyle().set("background-color", "#78808c");
        footer.getStyle().set("width", "100%");
        footer.add(div);
    }

    private void recentHeadLines() {
        for (Articles articles : apiService.getAllRecentHeadlines()) {
            Label title = new Label(articles.getTitle());
            title.getStyle().set("font-size", "13px");
            title.getStyle().set("font-weight", "bold");

            Paragraph titleParagraph = new Paragraph(articles.getTitle());
            titleParagraph.getStyle().set("font-weight","bold");
            recentTimeLineLayout.add(titleParagraph);

            Paragraph descParagraph = new Paragraph(articles.getDescription().replace(" | ESPNcricinfo.com", ""));
            recentTimeLineLayout.add(descParagraph);
            descParagraph.getStyle().set("font-size","15px");

            Anchor anchor = new Anchor();
            recentTimeLineLayout.add(anchor);
            anchor.setText("See More...");
            anchor.getElement().addEventListener("click", e -> anchor.setHref(articles.getUrl().toString()));
            anchor.getStyle().set("margin","0px");
            recentTimeLineLayout.add(new Hr());


        }
    }

    private void liveScores() {

    }

}

