package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.Route;
import frontend.model.*;
import frontend.service.CoachService;
import frontend.service.MatchService;
import frontend.service.PlayerService;
import frontend.service.TeamService;
import org.vaadin.gatanaso.MultiselectComboBox;

import java.util.Set;

public class MatchDayTeamForm extends VerticalLayout {
    private InsertMatchDayTeam insertMatchDayTeam;

    private ComboBox<Match> matchId = new ComboBox<>("Match");
    private MultiselectComboBox<Player> playerList = new MultiselectComboBox<>();
    private MultiselectComboBox<Player> substitutesList = new MultiselectComboBox<>();
    private MultiselectComboBox<Coach> coachList = new MultiselectComboBox<>();

    private Button save = new Button("Save");
    private Button delete = new Button("Delete");
    private ComboBox<Country> representingteam = new ComboBox<>("Team Id");
    private Binder<Team> binder = new Binder<>();

    private Team team = new Team();
    private PlayerService playerService = new PlayerService();
    private TeamService teamService = new TeamService();
    private CoachService coachService = new CoachService();
    private MatchService matchService = new MatchService();

    public MatchDayTeamForm(InsertMatchDayTeam insertMatchDayTeam) {
        this.insertMatchDayTeam = insertMatchDayTeam;

        HorizontalLayout buttons = new HorizontalLayout(save, delete);
        save.getElement().getThemeList().add("primary");

        add(matchId, representingteam, playerList,substitutesList, coachList, buttons);

        representingteam.setItems(Country.values());

        matchId.setItems(matchService.getAllMatch());
        matchId.setItemLabelGenerator(Match::getMatchId);
        playerList.setItems(playerService.getPlayerByPlayingTeam(representingteam.getValue()));
        playerList.setLabel("PlayerList");
        playerList.setItemLabelGenerator(Player::getFirstName);

        substitutesList.setEnabled(false);
        playerList.addValueChangeListener(e -> {
            if (e.getValue() == null){
                substitutesList.setEnabled(false);
            }else {
                substitutesList.setEnabled(true);
                substitutesList.setItems(playerService.getPlayerByPlayingTeam(representingteam.getValue()).stream().filter(value -> value != e.getValue()));
            }
        });
        substitutesList.setLabel("Substitutes PlayerList");
        substitutesList.setItemLabelGenerator(Player::getFirstName);

        coachList.setItems(coachService.getCoachByPlayingTeam(representingteam.getValue()));
        coachList.setLabel("CoachList");
        coachList.setItemLabelGenerator(Coach::getFirstName);

        binder.forField(representingteam)
                .asRequired("Id Required")
                .bind(Team::getTeamId, Team::setTeamId);
        binder.forField(playerList)
                .asRequired()
                .bind(Team::getPlayerSet,Team::setPlayerSet);
        binder.forField(substitutesList)
                .asRequired()
                .bind(Team::getSubstitutes,Team::setSubstitutes);
        binder.forField(coachList)
                .asRequired()
                .bind(Team::getCoachSet,Team::setCoachSet);

        save.addClickListener(e -> this.save());
        delete.addClickListener(e -> this.delete());
        save.setEnabled(false);
        binder.addStatusChangeListener(status -> {
            save.setEnabled(!status.hasValidationErrors());
        });

    }

    public void setTeam(Team team) {
        this.team = team;
        binder.readBean(team);

        // Show delete button for only customers already in the database
        delete.setVisible(team.isPersisted());
        setVisible(true);
        representingteam.focus();
    }

    private void delete() {
        teamService.delete(team);
        setVisible(false);
        notifyMainView();
    }

    private void notifyMainView() {
        if (insertMatchDayTeam != null) {
            Notification.show("Updated");
            insertMatchDayTeam.updateList();
        }
    }

    private void save() {
        Team savedTeam = new Team();
        try {
            binder.writeBean(savedTeam);
            if (teamService.getTeamById(savedTeam.getTeamId().toString()).isPresent()) {
                teamService.updateTeam(savedTeam);
                binder.readBean(new Team());
            } else {
                teamService.saveTeam(savedTeam);
                binder.readBean(new Team());
            }
        } catch (ValidationException e) {
            e.printStackTrace();
        }
        notifyMainView();
    }

    }

