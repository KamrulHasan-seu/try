package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import frontend.model.Coach;
import frontend.model.Player;
import frontend.model.Team;
import frontend.service.CoachService;
import frontend.service.PlayerService;
import frontend.service.TeamService;

import java.util.HashSet;
import java.util.Set;

@Route(value = "insertTeam",layout = AdminPanel.class)
public class InsertTeam extends VerticalLayout {
    public static final String VIEW_NAME = "Insert Team";
    private Grid<Team> grid = new Grid<>();
    private TextField filterText = new TextField();
    private TeamForm form = new TeamForm(this);

    private TeamService teamService = new TeamService();

    public InsertTeam() {
        filterText.setPlaceholder("filter by name...");
        filterText.addValueChangeListener(e -> updateList());
        filterText.setValueChangeMode(ValueChangeMode.EAGER);

        Button clearFilterTextBtn = new Button(VaadinIcon.CLOSE_CIRCLE_O.create());
        clearFilterTextBtn.getElement().setProperty("tooltip", "Clear the current filter");
        clearFilterTextBtn.addClickListener(e -> filterText.clear());

        HorizontalLayout filtering = new HorizontalLayout();
        filtering.add(filterText, clearFilterTextBtn);
        filtering.setSpacing(false);

        Button addCustomerBtn = new Button("Add new Team");

        addCustomerBtn.addClickListener(e -> {
            grid.asSingleSelect().clear();
            form.setTeam(new Team());
        });

        HorizontalLayout toolbar = new HorizontalLayout(filtering, addCustomerBtn);
        Set<Team> allTeam = teamService.getAllTeam();
        Grid.Column<Team> team_id = grid.addColumn(Team::getTeamId).setHeader("Team Id").setFooter("Total" + allTeam.size() + " teams");

        grid.addColumn(team -> {
            Set<String> coachNames = new HashSet<>();
            for (Coach coach : team.getCoachSet()){
                coachNames.add(coach.getFirstName());
            }
            return String.join(", ", coachNames);
        });

        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER,
                GridVariant.LUMO_NO_ROW_BORDERS, GridVariant.LUMO_ROW_STRIPES);


        HorizontalLayout main = new HorizontalLayout(grid, form);


        main.setSizeFull();
        main.setFlexGrow(1, grid);
        this.setSizeFull();
        add(toolbar, main);

        // fetch list of Customers from service and assign it to Grid
        updateList();
        form.setVisible(false);
        grid.asSingleSelect().addValueChangeListener(event -> {
            if (event.getValue() == null) {
                form.setVisible(false);
            } else {
                form.setTeam(event.getValue());
            }
        });
    }

//    private void itemsDetailsRenderer() {
//        grid.setItemDetailsRenderer(TemplateRenderer.<Team>of(
//                "<div class='custom-details' style='border: 1px solid gray; padding: 10px; width: 100%; box-sizing: border-box;'>"
//                        + "<div>Team name is <b>[[item.firstName]]</b> </div>"
//                        + "<div><b>Player List : </b>[[item.playerSet]] </div>"
//                        + "<div><b>Coach List : </b>[[item.coachList]] </div>"
//                        + "</div>")
//                .withProperty("firstName", Team::getTeamId)
//                .withProperty("playerSet", Team::getPlayerSet.toString())
//                .withProperty("coachlist", Team::getCoachSet)
//                // This is now how we open the details
//                .withEventHandler("handleClick", team -> {
//                    grid.getDataProvider().refreshItem(team);
//                }));
//
//// Disable the default way of opening item details:
//        grid.setDetailsVisibleOnClick(false);
//
//        grid.addColumn(new NativeButtonRenderer<>("Details", item -> grid
//                .setDetailsVisible(item, !grid.isDetailsVisible(item)))).setHeader("Details");



    public void updateList() {
        grid.setItems(teamService.getAllTeam().stream().filter(e -> e.getTeamId().toString().contains(filterText.getValue())));

    }
}
