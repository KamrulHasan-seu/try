package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import frontend.model.Coach;
import frontend.model.Country;
import frontend.service.CoachService;
import sun.applet.Main;

import java.util.HashSet;
import java.util.Set;

@Route(value = "insertCoach",layout = AdminPanel.class)
public class InsertCoach extends VerticalLayout  {
    public static final String VIEW_NAME = "Insert Coach";
    private CoachService coachService = new CoachService();
    private Grid<Coach> grid = new Grid<>(Coach.class);
    private TextField filterText = new TextField();
    private CoachForm form = new CoachForm(this);
    private Set<Country> coachesForSet = new HashSet<>();

    private CoachCrudLogic viewLogic = new CoachCrudLogic(this);

    public InsertCoach() {
        grid.setColumns("id","firstName","middleName","lastName","nationality","dateOfBirth","height","nowCoachingTeam","coachingStartsDate","coachingEndsDate");
        grid.addColumn(Coach::getCoachesFor).setHeader("CoachesFor");

        filterText.setPlaceholder("filter by name...");
        filterText.addValueChangeListener(e -> updateList());
        filterText.setValueChangeMode(ValueChangeMode.EAGER);

        Button clearFilterTextBtn = new Button(VaadinIcon.CLOSE_CIRCLE_O.create());
        clearFilterTextBtn.getElement().setProperty("tooltip", "Clear the current filter");
        clearFilterTextBtn.addClickListener(e -> filterText.clear());

        HorizontalLayout filtering = new HorizontalLayout();
        filtering.add(filterText, clearFilterTextBtn);
        filtering.setSpacing(false);

        Button addCustomerBtn = new Button("Add new coach");
        addCustomerBtn.addClickListener(e -> {
            grid.asSingleSelect().clear();
            form.setCoach(new Coach());
        });

        HorizontalLayout toolbar = new HorizontalLayout(filtering, addCustomerBtn);

        HorizontalLayout main = new HorizontalLayout(grid, form);

        main.setSizeFull();
        main.setFlexGrow(1, grid);
        this.setSizeFull();
        add(toolbar, main);

        // fetch list of Customers from service and assign it to Grid
        updateList();

        form.setVisible(false);

//        grid.asSingleSelect().addValueChangeListener(event -> {
//            if (event.getValue() == null) {
//                form.setVisible(false);
//            } else {
//                form.setCoach(event.getValue());
//            }
//        });
        grid.asSingleSelect().addValueChangeListener(e -> {
            viewLogic.rowSelected(e.getValue());
        });
    }

    public void updateList() {
        grid.setItems(coachService.getAllCoach().stream().filter(s -> s.getFirstName().contains(filterText.getValue())));
    }

    public void clearSelection() {
        grid.getSelectionModel().deselectAll();
    }
    public void selectRow(Coach row) {
        grid.getSelectionModel().select(row);
    }
    public void showForm(boolean show) {
        form.setVisible(show);

        /* FIXME The following line should be uncommented when the CheckboxGroup
         * issue is resolved. The category CheckboxGroup throws an
         * IllegalArgumentException when the form is disabled.
         */
        //form.setEnabled(show);
    }



    public void editProduct(Coach workModel) {
        showForm(workModel != null);
        form.setCoach(workModel);
    }

}

