package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.RouterLayout;
import frontend.security.AccessControlFactory;

@StyleSheet("styles.css")
public class AdminPanel extends VerticalLayout implements RouterLayout {
    public static final String VIEW_NAME = "Admin Panel";

    private Button insertBallButton = new Button("Insert Ball");
    private Button insertCoachButton = new Button("Insert Coach");
    private Button insertGroundButton = new Button("Insert Ground");
    private Button insertMatchButton = new Button("Insert Match");
    private Button insertPlayerButton = new Button("Insert Player");
    private Button insertTeamButton = new Button("Insert Team");
    private Button logOut = new Button("Log Out");
    private Button homeButton = new Button("Home");

    private HorizontalLayout navBar = new HorizontalLayout();
    private HorizontalLayout nav = new HorizontalLayout();
    private Button label = new Button("GameStats");

    public AdminPanel() {
        nav.add(label,navBar);
        this.add(nav);
        label.getStyle().set("font-weight", "bold");
        label.getStyle().set("font-size", "20px");
        label.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        label.addClickListener(e -> {
            label.getUI().ifPresent(ui -> ui.navigate(""));
        });

        nav.add(insertBallButton,insertCoachButton,insertGroundButton,insertMatchButton,insertTeamButton,insertPlayerButton,logOut,homeButton);

        //addMenuElement(InsertBall.class,"insertBall");
        
        insertBallButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        insertCoachButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        insertGroundButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        insertMatchButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        insertPlayerButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        insertTeamButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        logOut.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        homeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        navBar.addClassName("adminNav");
        nav.getStyle().set("width","100%");

        clickListener();

    }


    private void clickListener() {
        insertBallButton.addClickListener(e->{
            insertBallButton.getUI().ifPresent(ui -> ui.navigate(InsertBall.class));
        });

        insertPlayerButton.addClickListener(e->{
            insertBallButton.getUI().ifPresent(ui -> ui.navigate(InsertPlayer.class));
        });
        insertMatchButton.addClickListener(e->{
            insertBallButton.getUI().ifPresent(ui -> ui.navigate(InsertMatch.class));
        });
        insertGroundButton.addClickListener(e->{
            insertBallButton.getUI().ifPresent(ui -> ui.navigate(InsertGround.class));
        });
        insertCoachButton.addClickListener(e->{
            insertBallButton.getUI().ifPresent(ui -> ui.navigate(InsertCoach.class));
        });
        insertTeamButton.addClickListener(e->{
            insertBallButton.getUI().ifPresent(ui -> ui.navigate(InsertTeam.class));
        });
        logOut.addClickListener(e->{
            AccessControlFactory.getInstance().createAccessControl().signOut();

        });
        homeButton.addClickListener(e->{
            insertBallButton.getUI().ifPresent(ui -> ui.navigate(""));
        });


    }
}

