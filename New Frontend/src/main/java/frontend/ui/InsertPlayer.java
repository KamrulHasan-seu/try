package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.NativeButtonRenderer;
import com.vaadin.flow.data.renderer.TemplateRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import frontend.model.Person;
import frontend.model.Player;
import frontend.service.PlayerService;

import java.util.Set;

@Route(value = "insertPlayer",layout = AdminPanel.class)
public class InsertPlayer extends VerticalLayout {
    public static final String VIEW_NAME = "Insert Player";
    private PlayerService playerService = new PlayerService();
    private Grid<Player> grid = new Grid<>(Player.class);
    private TextField filterText = new TextField();
    private PlayerForm playerForm = new PlayerForm(this);

    public InsertPlayer() {

        filterText.setPlaceholder("filter by name...");
        filterText.addValueChangeListener(e -> updateList());
        filterText.setValueChangeMode(ValueChangeMode.EAGER);

        Button clearFilterTextBtn = new Button(VaadinIcon.CLOSE_CIRCLE_O.create());
        clearFilterTextBtn.getElement().setProperty("tooltip", "Clear the current filter");
        clearFilterTextBtn.addClickListener(e -> filterText.clear());

        HorizontalLayout filtering = new HorizontalLayout();
        filtering.add(filterText, clearFilterTextBtn);
        filtering.setSpacing(false);

        Button addCustomerBtn = new Button("Add new Player");
        addCustomerBtn.addClickListener(e -> {
            grid.asSingleSelect().clear();
            playerForm.setPlayer(new Player());
        });

        HorizontalLayout toolbar = new HorizontalLayout(filtering, addCustomerBtn);

        Set<Player> allPlayer = playerService.getAllPlayer();
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER,
                GridVariant.LUMO_NO_ROW_BORDERS, GridVariant.LUMO_ROW_STRIPES);
        Grid.Column<Player> playerId = grid.addColumn(Player::getPlayerId).setHeader("playerId");
        grid.appendFooterRow().getCell(playerId).setComponent(
                new Label("Total: " + allPlayer.size() + " people"));
        grid.setColumns( "firstName", "middleName", "lastName", "nationality", "dateOfBirth", "height", "jerseyNumber", "playerRole", "batsmanType", "bowlerType");
        itemsDetailsRenderer();

        HorizontalLayout main = new HorizontalLayout(grid, playerForm);


        main.setSizeFull();
        main.setFlexGrow(1, grid);
        this.setSizeFull();
        add(toolbar, main);

        // fetch list of Customers from service and assign it to Grid
        updateList();
        playerForm.setVisible(false);

        grid.asSingleSelect().addValueChangeListener(event -> {
            if (event.getValue() == null) {
                playerForm.setVisible(false);
            } else {
                playerForm.setPlayer(event.getValue());
            }
        });
    }

    private void itemsDetailsRenderer() {
        grid.setItemDetailsRenderer(TemplateRenderer.<Player>of(
                "<div class='custom-details' style='border: 1px solid gray; padding: 10px; width: 100%; box-sizing: border-box;'>"
                        + "<div>Hi! My name is <b>[[item.firstName]] [[item.middlename]] [[item.lastname]]</b> , <b>Nationality : </b>[[item.nationality]],</div>"
                        + "<div><b>Height : </b>[[item.height]] , <b>Jersey no:</b> [[item.jerseyno]] , <b>DOB : </b>[[item.dob]] </div>"
                        + "<div><b>Batsman Type : </b>[[item.batsmantype]], <b>BowlerType : </b>[[item.bowlertype]]</div>"
                        + "</div>")
                .withProperty("firstName", Person::getFirstName)
                .withProperty("middlename", Person::getMiddleName)
                .withProperty("lastname", Person::getLastName)
                .withProperty("nationality", Person::getNationality)
                .withProperty("dob", Person::getDateOfBirth)
                .withProperty("height", Person::getHeight)
                .withProperty("jerseyno", Player::getJerseyNumber)
                .withProperty("playerrole", Player::getPlayerRole)
                .withProperty("batsmantype", Player::getBatsmanType)
                .withProperty("bowlertype", Player::getBowlerType)
                // This is now how we open the details
                .withEventHandler("handleClick", player -> {
                    grid.getDataProvider().refreshItem(player);
                }));

// Disable the default way of opening item details:
        grid.setDetailsVisibleOnClick(false);

        grid.addColumn(new NativeButtonRenderer<>("Details", item -> grid
                .setDetailsVisible(item, !grid.isDetailsVisible(item)))).setHeader("Details");

    }

    public void updateList() {
        grid.setItems(playerService.getAllPlayer().stream().filter(s -> s.getFirstName().toLowerCase().contains(filterText.getValue().toLowerCase()))
                .filter(s -> s.getMiddleName().toLowerCase().contains(filterText.getValue().toLowerCase()))
                .filter(s -> s.getLastName().toLowerCase().contains(filterText.getValue().toLowerCase())));


    }


}

