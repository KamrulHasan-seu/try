package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import frontend.model.Ground;
import frontend.service.GroundService;


@Route(value = "insertground",layout = AdminPanel.class)
public class InsertGround extends VerticalLayout {
    public static final String VIEW_NAME = "Insert Ground";
    private GroundService groundService = new GroundService();
    private Grid<Ground> grid = new Grid<>(Ground.class);
    private TextField filterText = new TextField();
    private GroundForm form = new GroundForm(this);

    public InsertGround() {

        filterText.setPlaceholder("filter by name...");
        filterText.addValueChangeListener(e -> updateList());
        filterText.setValueChangeMode(ValueChangeMode.EAGER);

        Button clearFilterTextBtn = new Button(VaadinIcon.CLOSE_CIRCLE_O.create());
        clearFilterTextBtn.getElement().setProperty("tooltip", "Clear the current filter");
        clearFilterTextBtn.addClickListener(e -> filterText.clear());

        HorizontalLayout filtering = new HorizontalLayout();
        filtering.add(filterText, clearFilterTextBtn);
        filtering.setSpacing(false);

        Button addCustomerBtn = new Button("Add new ground");
        addCustomerBtn.addClickListener(e -> {
            grid.asSingleSelect().clear();
            form.setGround(new Ground());
        });

        HorizontalLayout toolbar = new HorizontalLayout(filtering, addCustomerBtn);
        grid.setColumns("groundId", "groundName", "city", "country", "capacity", "inaugurationDate");

        HorizontalLayout main = new HorizontalLayout(grid, form);


        main.setSizeFull();
        main.setFlexGrow(1, grid);
        this.setSizeFull();
        add(toolbar, main);

        // fetch list of Customers from service and assign it to Grid
        updateList();

        form.setVisible(false);

        grid.asSingleSelect().addValueChangeListener(event -> {
            if (event.getValue() == null) {
                form.setVisible(false);
            } else {
                form.setGround(event.getValue());
            }
        });
    }

    public void updateList() {
        grid.setItems(groundService.getAllGround().stream().filter(s -> s.getGroundName().contains(filterText.getValue())));
    }


}




