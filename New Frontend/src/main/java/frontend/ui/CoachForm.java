package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.converter.StringToDoubleConverter;
import org.vaadin.gatanaso.MultiselectComboBox;
import frontend.model.Coach;
import frontend.model.Country;
import frontend.service.CoachService;

public class CoachForm extends VerticalLayout {
    private CoachService coachService = new CoachService();
    private TextField firstName = new TextField("First Name", "First Name");
    private TextField middleName = new TextField("Middle Name", "Middle  Name");
    private TextField lastName = new TextField("Last Name", "Last Name");
    private TextField nationality = new TextField("Nationality");
    private DatePicker dob = new DatePicker("Date Of Birth");
    private TextField height = new TextField("Height", "Height");
    private TextField id = new TextField("Id", "Id");
    private ComboBox<Country> nowCoachingTeam = new ComboBox<>("CoachingTeam");
    private MultiselectComboBox<Country> coachesFor = new MultiselectComboBox<>();
    private DatePicker startCoachingDate = new DatePicker("StartCoachingDate");
    private DatePicker endsCoachingDate = new DatePicker("EndsCoachingDate");
    private Button save = new Button("Save");
    private Button delete = new Button("Delete");

    private InsertCoach mainUI;
    private Coach coach;
    private Binder<Coach> binder = new Binder<>(Coach.class);

    public CoachForm(InsertCoach mainUI) {
        this.mainUI = mainUI;
        nowCoachingTeam.setItems(Country.values());
        coachesFor.setItems(Country.values());
        coachesFor.setLabel("Coaches For");

        setSizeUndefined();
        HorizontalLayout buttons = new HorizontalLayout(save, delete);
        save.getElement().getThemeList().add("primary");
        add(firstName, middleName, lastName, nationality, dob, height, id, nowCoachingTeam, startCoachingDate, endsCoachingDate,coachesFor, buttons);

        binder.forField(firstName)
                .asRequired()
                .withValidator(s -> s.length() >= 3, "Name Should be 1 Character Long")
                .withValidator(s -> !s.isEmpty() && s != null, "Name Can not be Empty")
                .bind(Coach::getFirstName, Coach::setFirstName);
        binder.forField(middleName)
                .asRequired()
                .withValidator(s -> s.length() >= 2, "Name Should be 1 Character Long")
                .withValidator(s -> !s.isEmpty() && s != null, "Name Can not be Empty")
                .bind(Coach::getMiddleName, Coach::setMiddleName);
        binder.forField(lastName)
                .asRequired()
                .withValidator(s -> s.length() >= 3, "Name Should be 1 Character Long")
                .withValidator(s -> !s.isEmpty() && s != null, "Country Can not be Empty")
                .bind(Coach::getLastName, Coach::setLastName);

        binder.forField(nationality)
                .asRequired()
                .bind(Coach::getNationality, Coach::setNationality);
        binder.forField(height)
                .asRequired()
                .withConverter(
                        new StringToDoubleConverter("Must enter a Double Number"))
                .bind(Coach::getHeight, Coach::setHeight);
        binder.forField(id)
                .asRequired()
                .withValidator(s -> s.length() >= 3, "Id Should be 2 digits Long")
                .withValidator(s -> !s.isEmpty() && s != null, "Id Can not be Empty")
                .bind(Coach::getId, Coach::setId);
        binder.forField(nowCoachingTeam)
                .asRequired()
                .bind(Coach::getNowCoachingTeam, Coach::setNowCoachingTeam);
        binder.forField(coachesFor)
                .bind(Coach::getCoachesFor,Coach::setCoachesFor);
        binder.forField(startCoachingDate)
                .asRequired()
                .bind(Coach::getCoachingStartsDate, Coach::setCoachingStartsDate);
        binder.forField(endsCoachingDate)
                .bind(Coach::getCoachingEndsDate, Coach::setCoachingEndsDate);
        binder.forField(dob)
                .asRequired()
                .bind(Coach::getDateOfBirth, Coach::setDateOfBirth);


        save.addClickListener(e -> this.save());
        delete.addClickListener(e -> this.delete());
        save.setEnabled(false);
        binder.addStatusChangeListener(status -> {
            save.setEnabled(!status.hasValidationErrors());
        });
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
        binder.readBean(coach);

        // Show delete button for only customers already in the database
        delete.setVisible(coach.isPersisted());
        setVisible(true);
        id.focus();
    }

    private void delete() {
        coachService.delete(coach);
        setVisible(false);
        notifyMainView();
    }


    private void save() {
        Coach coach = new Coach();
        try {
            binder.writeBean(coach);
            if (coachService.getCoachById(coach.getId()).isPresent()) {
                coachService.updateCoach(coach);
                binder.readBean(new Coach());
            } else {
                coachService.saveCoach(coach);
                binder.readBean(new Coach());
            }
        } catch (ValidationException e) {
            e.printStackTrace();
        }

        notifyMainView();
    }

    private void notifyMainView() {
        if (mainUI != null) {
            Notification.show("Updated");
            mainUI.updateList();
        }

    }
}
