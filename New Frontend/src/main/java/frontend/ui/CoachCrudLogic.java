package frontend.ui;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Label;
import frontend.model.Coach;
import frontend.security.AccessControl;
import frontend.security.AccessControlFactory;
import frontend.service.CoachService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class CoachCrudLogic {
    private InsertCoach view;
    @Autowired
    private CoachService coachService;

    public CoachCrudLogic(InsertCoach insertCoach) {
        this.view = insertCoach;
    }

    public void enter(String productId) {
        if (productId != null && !productId.isEmpty()) {
            if (productId.equals("new")) {
                newWork();
            } else {
                // Ensure this is selected even if coming directly here from
                // login
                try {
                    String pid = productId;
                    Coach workModel = coachService.getCoachById(pid).get();
                    view.selectRow(workModel);
                } catch (Exception e) {
                }
            }
        } else {
            view.showForm(false);
        }
    }
    public void newWork() {
        view.clearSelection();
        setFragmentParameter("new");
        view.editProduct(new Coach());
    }
    private void setFragmentParameter(String productId) {
        String fragmentParameter;
        if (productId == null || productId.isEmpty()) {
            fragmentParameter = "";
        } else {
            fragmentParameter = productId;
        }

       // UI.getCurrent().navigate(InsertCoach.class, fragmentParameter);
    }
    public void rowSelected(Coach workModel) {
        if (AccessControlFactory.getInstance().createAccessControl()
                .isUserInRole(AccessControl.ADMIN_ROLE_NAME)) {
            editProduct(workModel);
        }
    }
    public void editProduct(Coach product) {
        if (product == null) {
            setFragmentParameter("");
        } else {
            setFragmentParameter(product.getId() + "");
        }
        view.editProduct(product);
    }
}
