package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import org.vaadin.gatanaso.MultiselectComboBox;
import frontend.model.Coach;
import frontend.model.Country;
import frontend.model.Player;
import frontend.model.Team;
import frontend.service.CoachService;
import frontend.service.PlayerService;
import frontend.service.TeamService;

import java.util.Set;

public class TeamForm extends VerticalLayout {
    private MultiselectComboBox<Player> playerList = new MultiselectComboBox<>();
    private MultiselectComboBox<Coach> coachList = new MultiselectComboBox<>();

    private Button save = new Button("Save");
    private Button delete = new Button("Delete");
    private Dialog dialog = new Dialog();
    private ComboBox<Country> representingteam = new ComboBox<>("Team Id");
    private Binder<Team> binder = new Binder<>();

    private Team team = new Team();
    private PlayerService playerService = new PlayerService();
    private TeamService teamService = new TeamService();
    private CoachService coachService = new CoachService();

    private InsertTeam insertTeam;

    public TeamForm(InsertTeam insertTeam) {
        this.insertTeam = insertTeam;

        HorizontalLayout buttons = new HorizontalLayout(save, delete);
        save.getElement().getThemeList().add("primary");

        add(representingteam, playerList, coachList, buttons);

        representingteam.setItems(Country.values());

        playerList.setItems(playerService.getAllPlayer());
        playerList.setLabel("PlayerList");
        playerList.setItemLabelGenerator(Player::getFirstName);

        Set<Coach> allCoach = coachService.getAllCoach();
        coachList.setItems(allCoach);
        coachList.setLabel("CoachList");
        coachList.setItemLabelGenerator(Coach::getFirstName);

        binder.forField(representingteam)
                .asRequired("Id Required")
                .bind(Team::getTeamId, Team::setTeamId);
        binder.forField(playerList)
                .asRequired()
                .bind(Team::getPlayerSet,Team::setPlayerSet);
        binder.forField(coachList)
                .asRequired()
                .bind(Team::getCoachSet,Team::setCoachSet);

        save.addClickListener(e -> this.save());
        delete.addClickListener(e -> this.delete());
        save.setEnabled(false);
        binder.addStatusChangeListener(status -> {
            save.setEnabled(!status.hasValidationErrors());
        });

    }

    public void setTeam(Team team) {
        this.team = team;
        binder.readBean(team);

        // Show delete button for only customers already in the database
        delete.setVisible(team.isPersisted());
        setVisible(true);
        representingteam.focus();
    }

    private void delete() {
        teamService.delete(team);
        setVisible(false);
        notifyMainView();
    }

    private void notifyMainView() {
        if (insertTeam != null) {
            Notification.show("Updated");
            insertTeam.updateList();
        }
    }

    private void save() {
        Team savedTeam = new Team();
        try {
            binder.writeBean(savedTeam);
            if (teamService.getTeamById(savedTeam.getTeamId().toString()).isPresent()) {
                teamService.updateTeam(savedTeam);
                binder.readBean(new Team());
            } else {
                teamService.saveTeam(savedTeam);
                binder.readBean(new Team());
            }
        } catch (ValidationException e) {
            e.printStackTrace();
        }
        notifyMainView();
    }

}
