package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import frontend.model.Match;
import frontend.service.MatchService;

@Route(value = "insertMatch",layout = AdminPanel.class)
public class InsertMatch extends VerticalLayout {
    public static final String VIEW_NAME = "Insert Match";
    private MatchService matchService = new MatchService();
    private Grid<Match> grid = new Grid<>(Match.class);
    private TextField filterText = new TextField();
    private MatchForm form = new MatchForm(this);

    public InsertMatch() {
        filterText.setPlaceholder("filter by name...");
        filterText.addValueChangeListener(e -> updateList());
        filterText.setValueChangeMode(ValueChangeMode.EAGER);

        Button clearFilterTextBtn = new Button(VaadinIcon.CLOSE_CIRCLE_O.create());
        clearFilterTextBtn.getElement().setProperty("tooltip", "Clear the current filter");
        clearFilterTextBtn.addClickListener(e -> filterText.clear());

        HorizontalLayout filtering = new HorizontalLayout();
        filtering.add(filterText, clearFilterTextBtn);
        filtering.setSpacing(false);

        Button addCustomerBtn = new Button("Add new Match");
        addCustomerBtn.addClickListener(e -> {
            grid.asSingleSelect().clear();
            form.setMatch(new Match());
        });

        HorizontalLayout toolbar = new HorizontalLayout(filtering, addCustomerBtn);
        grid.setColumns("matchId", "matchType", "ground.groundName", "team1.teamId", "team2.teamId", "homeTeam.teamId", "startMatchDate", "endMatchDate", "matchStartTime", "matchEndsTime");
        grid.getColumnByKey("team1.teamId").setHeader("Team1");
        grid.getColumnByKey("team2.teamId").setHeader("Team2");
        grid.getColumnByKey("homeTeam.teamId").setHeader("HomeTeam");

        HorizontalLayout main = new HorizontalLayout(grid, form);

        main.setSizeFull();
        main.setFlexGrow(1, grid);
        this.setSizeFull();
        add(toolbar, main);

        // fetch list of Customers from service and assign it to Grid
        updateList();

        form.setVisible(false);

      gridDataToForm();

    }

    private void gridDataToForm() {
        grid.asSingleSelect().addValueChangeListener(event -> {
            if (event.getValue() == null) {
                form.setVisible(false);
            } else {
                form.setMatch(event.getValue());
            }
        });
    }

    public void updateList() {
        grid.setItems(matchService.getAllMatch().stream().filter(s -> s.getMatchId().toLowerCase().contains(filterText.getValue().toLowerCase())));
    }
}
