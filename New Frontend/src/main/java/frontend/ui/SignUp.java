package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import frontend.model.Ground;
import frontend.model.User;
import frontend.service.UserService;

@Route(value = "signUp")
@PageTitle("Sign Up")
public class SignUp extends FormLayout {
    private TextField firstName = new TextField("FirstName","First Name");
    private TextField middleName = new TextField("MiddleName","Middle Name");
    private TextField lastName = new TextField("LastName","Last Name");
    private TextField userName = new TextField("UserName","User Name");
    private TextField email = new TextField("Email","Email");
    private DatePicker datePicker = new DatePicker("DateOfBirth");
    private PasswordField passwordField = new PasswordField("Password","Password");
    private PasswordField confirmPass = new PasswordField("confirmPassword","Confirm Password");
    private Button signUp = new Button("Sign Up");
    private Button back = new Button("Back");

    private UserService userService = new UserService();
    private Binder<User> binder = new com.vaadin.flow.data.binder.Binder<>(User.class);

    public SignUp() {
        HorizontalLayout buttonLayout = new HorizontalLayout(signUp, back);
        add(firstName,middleName,lastName,userName,email,datePicker,passwordField,confirmPass,buttonLayout);
        this.getStyle().set("margin","auto");
        this.getStyle().set("padding-top","10%");
        this.setWidth("50%");
        back.addClickListener(e ->
        {
            back.getUI().ifPresent(ui-> ui.navigate("login"));
        });
        binderMethods();
        signUp.addClickListener(e -> saveListener());

    }

    private void saveListener() {
        User  savedUser = new User();
        try {
            binder.writeBean(savedUser);
            if (userService.getUserById(savedUser.getId()).isPresent()) {
                userService.updateUser(savedUser);
                binder.readBean(new User());
            } else {
                userService.saveUser(savedUser);
                binder.readBean(new User());
            }
        } catch (ValidationException e) {
            e.printStackTrace();
        }
        Notification.show("Saved New User");
    }

    private void binderMethods() {
        binder.forField(firstName).asRequired().bind(User::getFirstName,User::setFirstName);
        binder.forField(middleName).asRequired().bind(User::getMiddleName,User::setMiddleName);
        binder.forField(lastName).asRequired().bind(User::getLastName,User::setLastName);
        binder.forField(userName).asRequired().bind(User::getUserName,User::setUserName);
        binder.forField(email).asRequired().withValidator(new EmailValidator("Please provide Valid Mail")).bind(User::getEmail,User::setEmail);
        binder.forField(datePicker).asRequired().bind(User::getDateOfBirth,User::setDateOfBirth);
        binder.forField(passwordField).asRequired().bind(User::getPassword,User::setPassword);
    }

}
