package frontend.ui;

import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import frontend.model.CommonType;
import frontend.service.BallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.gatanaso.MultiselectComboBox;

@StyleSheet("styles.css")
public class DetailsView extends VerticalLayout {
    private InsertBall insertBall;
    @Autowired
    private BallService ballService;
    private Label label = new Label("Last Ball");
    public Label run = new Label("Run");
//
    private TextField overNumber = new TextField("Over No", "0", "Over No");
    private TextField ballnumber = new TextField("Ball No", "1", "Ball No");
    private TextField ballSpeed = new TextField("Ball Speed", "0", "Ball Speed");
    private TextField strikerBatsman = new TextField("Striker Batsman");
    private TextField nonStrikerBatsman = new TextField("Non- Strike Batsman");
    private TextField runOutPlayer = new TextField("RunOut Player");
    private TextField bowler = new TextField("Bowler");
    private TextField ballTypeComboBox = new TextField("BowlType");
    private MultiselectComboBox<CommonType> commonType = new MultiselectComboBox<>();
    //private TextField run = new TextField("Run", "0", "Run");
    private TextArea commentary = new TextArea("Commentary");


    public DetailsView(InsertBall insertBall) {
        this.insertBall = insertBall;

        add(label);
        label.addClassName("detailsView-label");
        //add(strikerBatsman,nonStrikerBatsman,overNumber,ballnumber,ballSpeed,bowler,commonType,commentary);
        setDetails();
        setDesign();


        add(run);

    }

    private void setDesign() {

    }

    private void setDetails() {

    }
}
