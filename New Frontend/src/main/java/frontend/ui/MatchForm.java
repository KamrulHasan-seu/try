package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import frontend.model.Ground;
import frontend.model.Match;
import frontend.model.MatchType;
import frontend.model.Team;
import frontend.service.GroundService;
import frontend.service.MatchService;
import frontend.service.TeamService;


public class MatchForm extends FormLayout {
    private InsertMatch matchUI;
    private Match match;
    private Binder<Match> binder = new Binder<>();
    private MatchService matchService = new MatchService();
    private GroundService groundService = new GroundService();
    private TeamService teamService = new TeamService();

    private TextField id = new TextField("Match Id");
    private ComboBox<MatchType> matchTypeComboBox = new ComboBox<>("Match Type");
    private ComboBox<Ground> groundComboBox = new ComboBox<>("Ground");
    private ComboBox<Team> team1 = new ComboBox<>("Team1");
    private ComboBox<Team> team2 = new ComboBox<>("Team2");
    private ComboBox<Team> homeTeam = new ComboBox<>("Home Team");
    private DatePicker matchstartday = new DatePicker("Match Start Day");
    private DatePicker matchendsdate = new DatePicker("Match Ends Day");
    private TimePicker starttime = new TimePicker("Start Time");
    private TimePicker endstime = new TimePicker("Ends Time");
    private Button save = new Button("Save");
    private Button delete = new Button("Delete");

    public MatchForm(InsertMatch matchUI) {
        this.matchUI = matchUI;

        setSizeUndefined();
        HorizontalLayout buttons = new HorizontalLayout(save, delete);
        save.getElement().getThemeList().add("primary");
        add(id, matchTypeComboBox, groundComboBox,team1,team2, homeTeam, matchstartday, matchendsdate,
                starttime, endstime, buttons);

        team1.setItems(teamService.getAllTeam());
        //team1.setItemLabelGenerator(Team::getTeamId);
        team2.setItems(teamService.getAllTeam());
        homeTeam.setItems(teamService.getAllTeam());
        matchTypeComboBox.setItems(MatchType.values());
        groundComboBox.setItems(groundService.getAllGround());
        groundComboBox.setItemLabelGenerator(Ground::getGroundName);


        binder.forField(id)
                .asRequired()
                .bind(Match::getMatchId, Match::setMatchId);
        binder.forField(matchTypeComboBox)
                .asRequired()
                .bind(Match::getMatchType, Match::setMatchType);
        binder.forField(groundComboBox)
                .asRequired()
                .bind(Match::getGround, Match::setGround);
        binder.forField(team1)
                .asRequired()
                .bind(Match::getTeam1, Match::setTeam1);
        binder.forField(team2)
                .asRequired()
                .bind(Match::getTeam2, Match::setTeam2);
        binder.forField(homeTeam)
                .asRequired()
                .bind(Match::getHomeTeam, Match::setHomeTeam);
        binder.forField(matchstartday)
                .asRequired()
                .bind(Match::getStartMatchDate, Match::setStartMatchDate);
        binder.forField(matchendsdate)
                .asRequired()
                .bind(Match::getEndMatchDate, Match::setEndMatchDate);

        binder.forField(starttime).asRequired().bind(Match::getMatchStartTime,Match::setMatchStartTime);
        binder.forField(endstime).asRequired().bind(Match::getMatchEndsTime,Match::setMatchEndsTime);

        save.addClickListener(e -> this.save());
        delete.addClickListener(e -> this.delete());
        save.setEnabled(false);
        binder.addStatusChangeListener(status -> {
            save.setEnabled(!status.hasValidationErrors());
        });
    }


    public void setMatch(Match match) {
        this.match = match;
        binder.readBean(match);

        // Show delete button for only customers already in the database
        delete.setVisible(match.isPersisted());
        setVisible(true);
        id.focus();
    }

    private void delete() {
        matchService.delete(match);
        setVisible(false);
        notifyMainView();
    }

    private void notifyMainView() {
        if (matchUI != null) {
            Notification.show("Updated");
            matchUI.updateList();
        }
    }

    private void save() {
        Match savedMatch = new Match();
        try {

            binder.writeBean(savedMatch);

            if (matchService.getMatchById(savedMatch.getMatchId()).isPresent()) {
                matchService.updateMatch(savedMatch);
                binder.readBean(new Match());
            } else {
                matchService.saveMatch(savedMatch);
                binder.readBean(new Match());
            }
        } catch (ValidationException e) {
            e.printStackTrace();
        }

        notifyMainView();
    }

}
