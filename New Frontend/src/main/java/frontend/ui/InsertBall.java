package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.converter.StringToDoubleConverter;
import com.vaadin.flow.data.converter.StringToIntegerConverter;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.*;
import frontend.model.*;
import frontend.security.AccessControl;
import frontend.security.AccessControlFactory;
import frontend.service.BallService;
import frontend.service.MatchService;
import frontend.service.PlayerService;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.gatanaso.MultiselectComboBox;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Route(value = "insertball", layout = AdminPanel.class)
@StyleSheet("styles.css")
@Secured(AccessControl.ADMIN_ROLE_NAME)
public class InsertBall extends VerticalLayout  {
    public static final String VIEW_NAME = "Insert Ball";
    private List<Player> nullList = new ArrayList<>();
    private List<Innings> inningsList = new ArrayList<>();
    private List<Country> nullCountryList = new ArrayList<>();
    private MatchService matchService = new MatchService();
    private PlayerService playerService = new PlayerService();
    private BallService ballService = new BallService();
    private Ball ball;
    private BallDTO ballDTO = new BallDTO();

    private TextField ballId = new TextField("Ball Id");
    private ComboBox<Country> setBowlingTeam = new ComboBox<>("Bowling Team");
    public ComboBox<Country> setBattingTeam = new ComboBox<>("Batting Team");
    private ComboBox<Match> matchId = new ComboBox<>("Match Id");
    private ComboBox<Innings> inningsComboBox = new ComboBox<>("Innings");
    private TextField overNumber = new TextField("Over No", "0", "Over No");
    private TextField ballnumber = new TextField("Ball No", "1", "Ball No");
    private TextField ballSpeed = new TextField("Ball Speed", "0", "Ball Speed");
    private ComboBox<Player> strikerBatsman = new ComboBox<>("Striker Batsman");
    private ComboBox<Player> nonStrikerBatsman = new ComboBox<>("Non- Strike Batsman");
    private ComboBox<Player> runOutPlayer = new ComboBox<>("RunOut Player");
    private ComboBox<Player> bowler = new ComboBox<>("Bowler");
    private ComboBox<BallType> ballTypeComboBox = new ComboBox<>("BowlType");
    private MultiselectComboBox<CommonType> commonType = new MultiselectComboBox<>();
    private TextField run = new TextField("Run", "0", "Run");
    private TextArea commentary = new TextArea("Commentary", "", "");
    private Button save = new Button("Save");
    private Button close = new Button("Close");
    private Button update = new Button("Update");
    private Grid<Ball> ballGrid = new Grid<>(Ball.class);
    private TextField filterText = new TextField("", "Search by Over No");
    private Button closeButton = new Button(VaadinIcon.CLOSE_CIRCLE_O.create());
    private ComboBox<String> comment = new ComboBox<>("comment");

    //private Dialog dialog = new Dialog();

    private HorizontalLayout dividerLayout = new HorizontalLayout();
    private FormLayout form = new FormLayout();
    private VerticalLayout sideForm = new VerticalLayout();
    private HorizontalLayout buttonsLayout = new HorizontalLayout();
    private VerticalLayout batsmanLayout = new VerticalLayout();
    private VerticalLayout ballLayout = new VerticalLayout();
    private VerticalLayout dialogLayout = new VerticalLayout();
    private VerticalLayout detailsLayout = new VerticalLayout();
    private Binder<Ball> binder = new Binder();
    private HorizontalLayout filter = new HorizontalLayout();
    private HorizontalLayout commentaryLayout = new HorizontalLayout();
    private DetailsView detailsView = new DetailsView(this);

    public InsertBall() {

        add(dividerLayout);
        sideForm.add(setBattingTeam, setBowlingTeam, matchId, inningsComboBox, batsmanLayout);
        buttonsLayout.add(save, close, update);
        ballId.setVisible(false);
        update.setVisible(false);
        update.addClickListener(buttonClickEvent -> {
            this.updateBall();
        });
        batsmanLayout.add(strikerBatsman, nonStrikerBatsman, runOutPlayer);
        batsmanLayout.getStyle().set("padding", "0px");
        batsmanLayout.setVisible(false);

        dividerLayout.add(sideForm);
        detailsLayout.add(detailsView);
        detailsLayout.setWidth("50%");
        sideForm.setWidth("19%");
        commentaryLayout.add(comment, commentary);
        commentaryLayout.getElement().setAttribute("colspan", "2");
        commentary.setSizeFull();

        comment.setItems(commentList());
        comment.addValueChangeListener(e -> {
            if (!e.getValue().equals(null)) {
                commentary.setValue(e.getValue());
            } else {
                commentary.setValue("");
                comment.setItems(" ");
            }
        });


        form.add(ballId,overNumber, ballnumber, bowler, ballTypeComboBox, ballSpeed, run, commonType, commentaryLayout, buttonsLayout);

        ballLayout.add(new HorizontalLayout(form, detailsLayout), filter, ballGrid);
        filter.add(filterText, closeButton);
        closeButton.addClickListener(buttonClickEvent -> filterText.clear());
        filterText.setPlaceholder("filter by name...");
        filterText.addValueChangeListener(e -> updateList());
        filterText.setValueChangeMode(ValueChangeMode.EAGER);
        filter.setVisible(false);
        ballGrid.setVisible(false);
        //ballGrid.removeColumnByKey("ballId");
        ballGrid.removeColumnByKey("commentList");
        ballGrid.asSingleSelect().addValueChangeListener(gridBallComponentValueChangeEvent -> {
            if (!gridBallComponentValueChangeEvent.equals(null)) {
                update.setVisible(true);
                ballId.setVisible(true);
                this.setBall(gridBallComponentValueChangeEvent.getValue());
            } else {
                update.setVisible(false);
                ballId.setVisible(false);
            }
        });

        dividerLayout.add(ballLayout);
        detailsLayout.setVisible(false);
        commentary.getElement().setAttribute("colspan", "2");

        form.setWidth("40%");
        form.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("2.5em", 2));


        save.getElement().getThemeList().add("primary");
        close.getElement().getThemeList().add("error");

        inningsComboBox.setVisible(false);
        inningsComboBox.setItems(inningsList);
        matchId.addValueChangeListener(e -> {
            if (!e.getValue().getMatchType().equals(MatchType.TEST)) {
                inningsComboBox.setVisible(false);
            } else {
                inningsComboBox.setVisible(true);
                inningsComboBox.setItems(Innings.values());
            }
        });
        //LAYOUT MODIFIES ENDS

        Country[] countries = Country.values();
        setBattingTeam.setItems(countries);
        setBowlingTeam.setItems(nullCountryList);
        setBowlingTeam.setEnabled(false);

        setBattingTeam.addValueChangeListener(e -> {
            if (e.getValue() == null) {
                setBowlingTeam.setEnabled(false);
            } else {
                setBowlingTeam.setEnabled(true);
                setBowlingTeam.setItems(Arrays.stream(countries).filter(value -> value != e.getValue()));
            }
        });


        matchId.setItems(matchService.getAllMatch());
        matchId.setItemLabelGenerator(Match::getMatchId);

        //for preventing error for setting player in the batsman combo
        nullList.add(null);
        strikerBatsman.setItems(nullList);
        nonStrikerBatsman.setItems(nullList);

        bowler.setItems(nullList);
        bowler.setItemLabelGenerator(Player::getFirstName);

        strikerBatsman.setItemLabelGenerator(Player::getFirstName);
        nonStrikerBatsman.setItemLabelGenerator(Player::getFirstName);
        runOutPlayer.setItemLabelGenerator(Player::getFirstName);
        runOutPlayer.setItems(nullList);
        runOutPlayer.setEnabled(false);
        ballTypeComboBox.setItems(BallType.values());

        commonType.setItems(CommonType.values());
        commonType.setLabel("Run Type");
        commonType.getElement().setAttribute("colspan", "2");


        form.setVisible(false);
        //open form when all condition forms correctly
        openForm();

        commonType.addValueChangeListener(c -> {
            if (c.getValue() != null) {
                Set<CommonType> value = c.getValue();
                for (CommonType out : value) {
                    if (out.toString().equals("Run Out") || out.toString().equals("Bowled") || out.toString().equals("LBW") || out.toString().equals("Stumped")
                            || out.toString().equals("Caught")) {
                        runOutPlayer.setEnabled(true);
                    } else {
                        runOutPlayer.setEnabled(false);
                    }
                }
            } else {
                runOutPlayer.setEnabled(false);
            }

        });
        save.setEnabled(false);
        save.addClickListener(e -> saveBall());
        closeListener();

        //customDialog();
        binding();

    }

    private void detailsViewMethod() {
        detailsView.run.setText(run.getValue());
    }

    private List<String> commentList() {
        List<String> comments = new ArrayList<>();

        comments.add("What a 6");
        comments.add("How good is that shot");
        comments.add("Marvelous shot");
        comments.add("Balls are coming too fast,batsman looks uncomfortable at this pitch");
        comments.add("In-swinging delivery");
        comments.add("swing and a miss");
        comments.add("THAT'S OUT!! Caught&Bowled!!");
        return comments;

    }


//    private void customDialog() {
//        Label label = new Label("Are you sure?");
//
//        dialog.add(dialogLayout);
//
//        Button ok = new Button("Ok", event -> {
//            saveBall();
//            dialog.close();
//        });
//        Button cancel = new Button("Cancel", cancelEvent -> {
//            dialog.close();
//        });
//
//
//        ok.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
//        cancel.addThemeVariants(ButtonVariant.LUMO_ERROR);
//        dialogLayout.add(label, new HorizontalLayout(ok, cancel));
//        dialog.setWidth("400px");
//        dialog.setHeight("150px");
//    }


    private void binding() {
        binder.forField(matchId).asRequired().bind(Ball::getMatchId, Ball::setMatchId);
        binder.forField(ballId).asRequired().bind(Ball::getBallId, Ball::setBallId);
        binder.forField(overNumber).asRequired()
                .withConverter(new StringToIntegerConverter("Please Input this"))
                .bind(Ball::getOverNumber, Ball::setOverNumber);
        binder.forField(ballnumber).asRequired()
                .withConverter(new StringToIntegerConverter("Please Input this"))
                .bind(Ball::getBallNumber, Ball::setBallNumber);
        binder.forField(bowler).asRequired().bind(Ball::getBowler, Ball::setBowler);
        binder.forField(strikerBatsman).asRequired().bind(Ball::getBatsman, Ball::setBatsman);
        binder.forField(ballTypeComboBox).asRequired().bind(Ball::getBallType, Ball::setBallType);
        binder.forField(ballSpeed).asRequired()
                .withConverter(new StringToDoubleConverter("Please Input this"))
                .bind(Ball::getBallSpeed, Ball::setBallSpeed);
        binder.forField(commentary)
                .asRequired()
                .bind(Ball::getCommentary, Ball::setCommentary);


        strikerBatsman.setRequired(true);
        nonStrikerBatsman.setRequired(true);
        run.setRequired(true);
        binder.addStatusChangeListener(status -> {
            save.setEnabled(!status.hasValidationErrors());
        });
    }


    private void closeListener() {
        close.addClickListener(buttonClickEvent -> {
            this.setBall(new Ball());
        });
    }

    private void openForm() {
        matchId.addValueChangeListener(e -> {
            if (!setBattingTeam.isEmpty() && !setBowlingTeam.isEmpty() && !matchId.isEmpty()) {
                form.setVisible(true);
                batsmanLayout.setVisible(true);

                strikerBatsman.setItems(playerList());
                nonStrikerBatsman.setEnabled(false);
                detailsLayout.setVisible(true);
                strikerBatsman.addValueChangeListener(p -> {
                    if (p.getValue() == null) {
                        nonStrikerBatsman.setEnabled(false);
                    } else {
                        List<Player> playerByPlayingTeam = playerService.getPlayerByPlayingTeam(setBattingTeam.getValue());
                        nonStrikerBatsman.setEnabled(true);
                        nonStrikerBatsman.setItems(playerByPlayingTeam.stream().filter(player -> !player.equals(p.getValue())));
                    }
                });
                filter.setVisible(true);
                ballGrid.setVisible(true);
                ballGrid.setItems(ballService.getAllBymatchId(matchId.getValue().getMatchId()));
                runOutPlayer.setItems(playerList());
                bowler.setItems(playerService.getPlayerByPlayingTeam(setBowlingTeam.getValue()));

                runOutPlayer.addValueChangeListener(comboBoxPlayerComponentValueChangeEvent -> {

                    if (runOutPlayer.getValue().equals(strikerBatsman.getValue())) {
                        playerList().remove(comboBoxPlayerComponentValueChangeEvent.getValue());
                        strikerBatsman.setItems(playerList().stream().filter(player -> !player.equals(comboBoxPlayerComponentValueChangeEvent.getValue())));
                    } else {
                        playerList().remove(comboBoxPlayerComponentValueChangeEvent.getValue());
                        nonStrikerBatsman.setItems(playerList().stream().filter(player -> !player.equals(comboBoxPlayerComponentValueChangeEvent.getValue())));
                    }
                });
            } else {
                batsmanLayout.setVisible(false);
                form.setVisible(false);
                filter.setVisible(false);
                ballGrid.setVisible(false);
            }
        });
    }


    private void saveBall() {
        Match matchIdValue = matchId.getValue();
        int overNo = Integer.parseInt(overNumber.getValue());
        int ballNo = Integer.parseInt(ballnumber.getValue());
        Player bowlerValue = bowler.getValue();
        Player batsmanValue = strikerBatsman.getValue();
        String commentaryValue = commentary.getValue();
        BallType ballType = ballTypeComboBox.getValue();
        double ballSpeedValue = Double.parseDouble(ballSpeed.getValue());
        LocalDateTime dateTime = LocalDateTime.now();


        Ball ball = new Ball(matchIdValue, dateTime, overNo, ballNo,
                bowlerValue, batsmanValue, ballType, ballSpeedValue, commentaryValue);

        BallDTO ballDTO = getPairType(commonType.getValue());
        ballDTO.setOutPlayer(runOutPlayer.getValue());
        matchId.getValue().setInnings(inningsComboBox.getValue());
        ballDTO.setInnings(inningsComboBox.getValue());
        ballDTO.setBall(ball);
        System.out.println(ballDTO);
        ballService.saveBall(ballDTO);
        Notification.show("Insertion Completed");

        ballAndBatsmanChanging();
        ballTypeComboBox.setValue(null);

        detailsViewMethod();

    }


    private BallDTO getPairType(Set<CommonType> value) {
        BallDTO dto = new BallDTO();
        List<String> extra = new ArrayList<>();
        dto.setRunType(run.getValue());

        for (CommonType type : value) {
            for (OutType outType : OutType.values()) {
                if (type.name().equals(outType.name())) {
                    dto.setOutType(type.name());
                }
            }
            for (ExtraType extraType : ExtraType.values()) {
                if (type.name().equals(extraType.name())) {
                    extra.add(extraType.name());
                }
                dto.setExtraType(extra);
            }
        }

        return dto;
    }

    private void ballAndBatsmanChanging() {
        int ballNo = Integer.parseInt(ballnumber.getValue());
        ballnumber.setValue(String.valueOf(Math.incrementExact(ballNo)));
        if (ballNo == 6) {
            String valueOfOver = String.valueOf(Math.incrementExact(Integer.parseInt(overNumber.getValue())));
            overNumber.setValue(valueOfOver);
            ballnumber.setValue(String.valueOf(0));
            bowler.setValue(null);
        }
        if (run.getValue().equals(String.valueOf(1)) || run.getValue().equals(String.valueOf(3))) {
            Player nonStrikerBatsmanValue = nonStrikerBatsman.getValue();
            nonStrikerBatsman.setValue(strikerBatsman.getValue());
            strikerBatsman.setValue(nonStrikerBatsmanValue);
        }

    }

    public void updateList() {

        ballGrid.setItems(ballService.getAllBymatchId(matchId.getValue().getMatchId()).stream().filter(searchBall -> String.valueOf(searchBall.getOverNumber()).contains(filterText.getValue())));
    }

    public void setBall(Ball ball) {
        this.ball = ball;
        binder.readBean(ball);

        // Show delete button for only customers already in the database
        //delete.setVisible(coach.isPersisted());
        setVisible(true);
        ballTypeComboBox.focus();
    }


    private void updateBall() {
        Match matchIdValue = matchId.getValue();
        String ballIdValue = ballId.getValue();
        int overNo = Integer.parseInt(overNumber.getValue());
        int ballNo = Integer.parseInt(ballnumber.getValue());
        Player bowlerValue = bowler.getValue();
        Player batsmanValue = strikerBatsman.getValue();
        String commentaryValue = commentary.getValue();
        BallType ballType = ballTypeComboBox.getValue();
        double ballSpeedValue = Double.parseDouble(ballSpeed.getValue());
        LocalDateTime dateTime = LocalDateTime.now();


        Ball ball = new Ball(ballIdValue,matchIdValue, dateTime, overNo, ballNo,
                bowlerValue, batsmanValue, ballType, ballSpeedValue, commentaryValue);

        BallDTO ballDTO = getPairType(commonType.getValue());
        ballDTO.setOutPlayer(runOutPlayer.getValue());
        matchId.getValue().setInnings(inningsComboBox.getValue());
        ballDTO.setInnings(inningsComboBox.getValue());
        ballDTO.setBall(ball);
        ballService.updateBall(ballDTO);

        binder.readBean(new Ball());
        Notification.show("Updated Successfully");
    }

    private List<Player> playerList() {
        List<Player> playerByPlayingTeam = playerService.getPlayerByPlayingTeam(setBattingTeam.getValue());
        return playerByPlayingTeam;
    }


}

