package frontend.ui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import frontend.model.Coach;
import frontend.model.Match;
import frontend.model.Player;
import frontend.model.Team;
import frontend.service.MatchService;
import frontend.service.TeamService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Route
public class InsertMatchDayTeam extends VerticalLayout {
    private ComboBox<Match> matchId = new ComboBox<>("Match");
    private RadioButtonGroup<Team> group = new RadioButtonGroup<>();
    private Grid<Team> grid = new Grid<>();
    private TextField filterText = new TextField();
    private MatchDayTeamForm form = new MatchDayTeamForm(this);
    private HorizontalLayout matchLayout = new HorizontalLayout();

    private TeamService teamService = new TeamService();
    private MatchService matchService = new MatchService();
    private List<Team> nullList = new ArrayList<>();

    public InsertMatchDayTeam() {
        filterText.setPlaceholder("filter by name...");
        filterText.addValueChangeListener(e -> updateList());
        filterText.setValueChangeMode(ValueChangeMode.EAGER);

        Button clearFilterTextBtn = new Button(VaadinIcon.CLOSE_CIRCLE_O.create());
        clearFilterTextBtn.getElement().setProperty("tooltip", "Clear the current filter");
        clearFilterTextBtn.addClickListener(e -> filterText.clear());

        HorizontalLayout filtering = new HorizontalLayout();
        filtering.add(filterText, clearFilterTextBtn);
        filtering.setSpacing(false);

        Button addCustomerBtn = new Button("Add new Match Day Team");

        addCustomerBtn.addClickListener(e -> {
            grid.asSingleSelect().clear();
            form.setTeam(new Team());
        });

        matchId.setItems(matchService.getAllMatch());
        matchId.setItemLabelGenerator(Match::getMatchId);

        group.setItems();
        matchId.addValueChangeListener(e ->{
            if (e.getValue() == null){
                group.setEnabled(false);
            }else {
                group.setEnabled(true);
                group.setItems(matchId.getValue().getTeam1());
                group.setItems(matchId.getValue().getTeam2());
            }
        });


        HorizontalLayout toolbar = new HorizontalLayout(filtering, addCustomerBtn);

        grid.addColumn(Team::getTeamId).setHeader("Team");
        grid.addColumn(team -> {
            Set<String> coachNames = new HashSet<>();
            for (Coach coach : team.getCoachSet()) {
                coachNames.add(coach.getFirstName());
            }
            return String.join(", ", coachNames);
        }).setHeader("Coaches");

//        grid.addColumn(team -> {
//            Set<String> subsNames = new HashSet<>();
//            for (Player player : team.getSubstitutes()){
//                subsNames.add(player.getFirstName());
//            }
//            return String.join(", ", subsNames);
//        }).setHeader("Substitutes");

        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER,
                GridVariant.LUMO_NO_ROW_BORDERS, GridVariant.LUMO_ROW_STRIPES);


        HorizontalLayout main = new HorizontalLayout(grid, form);


        grid.setSizeFull();
        main.setSizeFull();
        main.setFlexGrow(1, grid);
        this.setSizeFull();

        add(toolbar, matchLayout, main);
        matchLayout.add(matchId,group);

        // fetch list of Customers from service and assign it to Grid
        updateList();
        form.setVisible(false);

        grid.asSingleSelect().addValueChangeListener(event -> {
            if (event.getValue() == null) {
                form.setVisible(false);
            } else {
                form.setTeam(event.getValue());
            }
        });
    }

    public void updateList() {
        // grid.setItems(teamService.getAllTeam().stream().filter(e -> e.getTeamId().toString().contains(filterText.getValue())));
        //grid.setItems(matchComboBox.getValue().getTe);

    }


}
