package frontend.ui;

import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import frontend.model.home.Matches;
import frontend.model.home.MatchesScheduleModel;
import frontend.service.ApiService;

@Route
public class NewMatchesScheduleUI extends VerticalLayout {
    private MatchesScheduleModel matchesScheduleModel = new MatchesScheduleModel();
    private ApiService apiService = new ApiService();

    private VerticalLayout layout = new VerticalLayout();
    public NewMatchesScheduleUI() {
        layout.add(new H2("All Matches"));
        add(layout);
        fixtures();
    }

    private void fixtures() {
        for (Matches matches : apiService.getAllNewMatches()) {
            Label team1 = new Label(matches.getTeam1());
            Label team2 = new Label(matches.getTeam2());
            layout.add(team1,team2);

        }
    }
}
