package frontend.ui;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.AppLayoutMenu;
import com.vaadin.flow.component.applayout.AppLayoutMenuItem;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.Command;
import frontend.security.AccessControlFactory;

@Viewport("width = device-width, minimum-scale=1.0, initial-scale =1.0, user-scalable=yes")
public class MainView extends VerticalLayout implements RouterLayout  {

    private HomeView homeView = new HomeView();
    private AppLayout appLayout = new AppLayout();
    private AppLayoutMenu menu = appLayout.createMenu();
    private Tabs tabs = new Tabs();

    private HorizontalLayout routerLayout = new HorizontalLayout();
    public MainView() {
        headerDiv();
        //appLayout.setContent(homeView);
        add(appLayout);
        add(routerLayout);
    }

    private void headerDiv() {
        Label label = new Label("GameStats");
        label.addClassName("mainTitle");
        label.getStyle().set("font-weight", "bold");
        label.getElement().getStyle().set("font-size", "30px");
        appLayout.setBranding(label);

        menu.addMenuItems(
                new AppLayoutMenuItem(VaadinIcon.HOME.create(), "Home", "homeView"),
                new AppLayoutMenuItem(VaadinIcon.COINS.create(), "Live Scores"),
                new AppLayoutMenuItem(VaadinIcon.LIST_SELECT.create(), "Schedule","newSchedule"),
                new AppLayoutMenuItem(VaadinIcon.RECORDS.create(), "Archives"),
                new AppLayoutMenuItem(VaadinIcon.NEWSPAPER.create(), "News"),
                new AppLayoutMenuItem(VaadinIcon.TROPHY.create(), "Series"),
                new AppLayoutMenuItem(VaadinIcon.USER_CARD.create(), "Team"),
                new AppLayoutMenuItem(VaadinIcon.SIGN_IN.create(), "Log In","login"));


    }


    @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);

//        attachEvent.getUI()
//                .addShortcutListener(
//                        () -> AccessControlFactory.getInstance()
//                                .createAccessControl().signOut(),
//                        Key.KEY_L, KeyModifier.CONTROL);

        // add the admin view menu item if/when it is registered dynamically
        Command addAdminMenuItemCommand = () -> this.addView(AdminPanel.class,
                AdminPanel.VIEW_NAME, VaadinIcon.DOCTOR.create());

        RouteConfiguration sessionScopedConfiguration = RouteConfiguration
                .forSessionScope();


        if (sessionScopedConfiguration.isRouteRegistered(AdminPanel.class) ) {
            addAdminMenuItemCommand.execute();

        }

        else {
            sessionScopedConfiguration.addRoutesChangeListener(event -> {
                for (RouteBaseData data : event.getAddedRoutes()) {
                    if (data.getNavigationTarget().equals(AdminPanel.class)) {
                        addAdminMenuItemCommand.execute();
                    }
                }
            });
        }
    }

    public void addView(Class<? extends Component> viewClass, String caption, Icon icon) {

        RouterLink routerLink = new RouterLink(null, viewClass);
        routerLink.add(icon);
        routerLink.add(new Span(caption));
        menu.addMenuItem(new AppLayoutMenuItem(routerLink));
    }
}
