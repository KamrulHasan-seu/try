package frontend.MatchFinder;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@lombok.Data
public class MatchModel {
    private int creditsLeft;
    private Provider provider;
    private int ttl;
    private String v;
    private boolean cache;
    private List<Data> data;

}
