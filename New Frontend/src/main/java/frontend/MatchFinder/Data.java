package frontend.MatchFinder;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@lombok.Data
public class Data {
    private String unique_id;
    private String name;
    private String date;

}
