package frontend.MatchFinder.repository;

import frontend.MatchFinder.Data;
import frontend.MatchFinder.MatchModel;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class ApiReader implements OnlineRedearDao {
    @Override
    public List<Data> listAllMatch() {
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<MatchModel> responseEntity = restTemplate.getForEntity("https://cricapi.com/api/matchCalendar?apikey=8roPV1IHd6glgHxIkFswOFQBCTC3",MatchModel.class);
        MatchModel matchModel = responseEntity.getBody();
        return matchModel.getData();
    }

    @Override
    public List<Data> getMatchByName(String name) {
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<MatchModel> responseEntity = restTemplate.exchange(
                "https://cricapi.com/api/matchCalendar?apikey=8roPV1IHd6glgHxIkFswOFQBCTC3&name="+name,
                HttpMethod.GET, null, new ParameterizedTypeReference<MatchModel>() {
                }
        );
        MatchModel matchModel = responseEntity.getBody();
        return matchModel.getData();
    }
}
