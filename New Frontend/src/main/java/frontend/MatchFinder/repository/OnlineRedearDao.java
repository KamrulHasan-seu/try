package frontend.MatchFinder.repository;

import frontend.MatchFinder.Data;

import java.util.List;

public interface OnlineRedearDao {
    public List<Data> listAllMatch();
    public List<Data> getMatchByName(String name);
}
