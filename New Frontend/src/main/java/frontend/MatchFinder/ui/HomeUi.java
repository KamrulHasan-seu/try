package frontend.MatchFinder.ui;

import com.vaadin.flow.component.HtmlComponent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Main;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.ParentLayout;
import com.vaadin.flow.router.Route;
import frontend.MatchFinder.Data;
import frontend.MatchFinder.repository.ApiReader;
import frontend.MatchFinder.repository.OnlineRedearDao;
import frontend.ui.MainView;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.stream.Collectors;

@Route(value = "allMatch")
@ParentLayout(MainView.class)
@StyleSheet("styles.css")
public class HomeUi extends VerticalLayout {

    private TextField search = new TextField();
    private Button button = new Button(VaadinIcon.CROSS_CUTLERY.create());

    private HorizontalLayout searchLayout = new HorizontalLayout();

    private OnlineRedearDao apiReader = new ApiReader();
    public HomeUi() {



        searchLayout.add(search, button);
        add(searchLayout);

        List<Data> data = apiReader.listAllMatch();

        for (Data data1 : data) {

           Div outer = new Div();
           Div inner = new Div();
            outer.add(data1.getDate());
            outer.add(new Hr());
            outer.add(inner);
            inner.add(data1.getName());

            add(outer);
            outer.addClassName("outer");
            inner.addClassName("inner");
        }

        button.addClickListener(e -> search.clear());
        search.setValueChangeMode(ValueChangeMode.EAGER);
        search.addValueChangeListener(e -> updateList());

    }

    private void updateList() {
//        List<Data> matchByName = apiReader.getMatchByName(search.getValue()).stream().filter(data -> data.getName().contains(search.getValue())).collect(Collectors.toList());
        List<Data> dataList = apiReader.listAllMatch().stream().filter(data -> data.getName().contains(search.getValue())).collect(Collectors.toList());
        for ( Data data1 :dataList) {
            Div outer = new Div();
            outer.removeAll();
            Div inner = new Div();
            inner.removeAll();
            outer.add(data1.getDate());
            outer.add(new Hr());
            outer.add(inner);
            inner.add(data1.getName());


            add(outer);
            outer.addClassName("outer");
            inner.addClassName("inner");
        }
    }



}
