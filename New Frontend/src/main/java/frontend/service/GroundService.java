package frontend.service;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import frontend.model.Ground;

import java.util.List;
import java.util.Optional;

public class GroundService {
    private String baseURL = "http://localhost:8081/api/ground";

    public List<Ground> getAllGround() {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Ground>> groundResponseEntity = restTemplate.exchange(
                baseURL + "/all",
                HttpMethod.GET,
                new HttpEntity<>(null),
                new ParameterizedTypeReference<List<Ground>>() {
                });

        return groundResponseEntity.getBody();
    }

    public Ground getGround(String name) {
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Ground> groundResponseEntity = restTemplate.exchange(
                baseURL + "/all/" + name,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Ground>() {
                });

        return groundResponseEntity.getBody();
    }

    public Optional<Ground> getGroundById(String id) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Ground> groundResponseEntity = null;

        try {
            groundResponseEntity = restTemplate.exchange(
                    baseURL + "/" + id,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<Ground>() {
                    });

            if (groundResponseEntity.getStatusCode() == HttpStatus.OK) {
                return Optional.of(groundResponseEntity.getBody());
            } else {
                return Optional.empty();
            }
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public void delete(Ground ground) {
        RestTemplate restTemplate = new RestTemplate();
        String id = ground.getGroundId();
        ResponseEntity<String> groundResponseEntity = restTemplate.exchange(
                baseURL + "/delete/" + id,
                HttpMethod.DELETE,
                new HttpEntity<>(null),
                new ParameterizedTypeReference<String>() {
                });

    }
    public Optional<Ground> saveGround(Ground ground) {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Ground> groundResponseEntity = null;

        groundResponseEntity = restTemplate.exchange(
                baseURL + "/save",
                HttpMethod.POST,
                new HttpEntity<>(ground),
                new ParameterizedTypeReference<Ground>() {
                });

        return Optional.of(groundResponseEntity.getBody());

    }

    public void updateGround(Ground ground) {
        RestTemplate restTemplate = new RestTemplate();
        String id = ground.getGroundId();
        HttpEntity<Ground> groundHttpEntity = new HttpEntity<>(ground, null);
        restTemplate.put(baseURL + "/update/" + id, groundHttpEntity, Ground.class);
    }
}
