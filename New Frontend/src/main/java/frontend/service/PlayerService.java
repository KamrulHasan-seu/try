package frontend.service;

import frontend.model.Country;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import frontend.model.Player;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public class PlayerService {
    private String baseURL = "http://localhost:8081/api/player";

    public Set<Player> getAllPlayer() {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Set<Player>> playerResponseEntity = restTemplate.exchange(
                baseURL + "/all",
                HttpMethod.GET,
                new HttpEntity<>(null),
                new ParameterizedTypeReference<Set<Player>>() {
                });
        return playerResponseEntity.getBody();
    }

    public List<Player> getPlayerByPlayingTeam(Country s) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Player>> playerResponseEntity = restTemplate.exchange(
                baseURL + "/allbyplayingteam/"+ s,
                HttpMethod.GET,
                new HttpEntity<>(null),
                new ParameterizedTypeReference<List<Player>>() {
                });
        return playerResponseEntity.getBody();
    }

    public Optional<Player> getPlayerById(String id) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Player> playerResponseEntity = null;

        try {
            playerResponseEntity = restTemplate.exchange(
                    baseURL + "/" + id,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<Player>() {
                    });
            if (playerResponseEntity.getStatusCode() == HttpStatus.OK) {
                return Optional.of(playerResponseEntity.getBody());
            } else {
                return Optional.empty();
            }
        } catch (Exception e) {
            return Optional.empty();
        }


    }

    public void delete(Player player) {
        RestTemplate restTemplate = new RestTemplate();
        String id = player.getPlayerId();
        ResponseEntity<String> playerResponseEntity = restTemplate.exchange(
                baseURL + "/delete/" + id,
                HttpMethod.DELETE,
                new HttpEntity<>(null),
                new ParameterizedTypeReference<String>() {
                });

    }

    public Optional<Player> savePlayer(Player player) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Player> playerResponseEntity = restTemplate.exchange(
                baseURL + "/save",
                HttpMethod.POST,
                new HttpEntity<>(player),
                new ParameterizedTypeReference<Player>() {
                });
        return Optional.of(playerResponseEntity.getBody());
    }


    public void updatePlayer(Player player) {
        RestTemplate restTemplate = new RestTemplate();
        String id = player.getPlayerId();
        HttpEntity<Player> playerHttpEntity = new HttpEntity<>(player, null);
        restTemplate.put(baseURL + "/update/" + id, playerHttpEntity, Player.class);
    }


}
