package frontend.service;

import frontend.model.Country;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import frontend.model.Coach;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public class CoachService {
    private String baseURL = "http://localhost:8081/api/coach";
    private String token = "Token eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJLYW1ydWwiLCJyb2xlcyI6W3siaWQiOiI1Y2E4NWMxYzY0NDA0YjA1Yjg5YjJiMzYiLCJyb2xlcyI6IkFETUlOIn1dLCJ1c2VyTmFtZSI6IkthbXJ1bCIsImV4cCI6MTU1NTQwMTUxMH0.V4vUwjw-xP0fRiEDjilYtvBaMe31ZKvQ6dggRT2sTZU";

    public Set<Coach> getAllCoach() {

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", token);

        ResponseEntity<Set<Coach>> coachResponseEntity = restTemplate.exchange(
                baseURL + "/all",
                HttpMethod.GET,
                new HttpEntity<>(headers),
                new ParameterizedTypeReference<Set<Coach>>() {
                });

        return coachResponseEntity.getBody();
    }
    public void delete(Coach coach) {
        RestTemplate restTemplate = new RestTemplate();
        String id = coach.getId();
        ResponseEntity<String> coachResponseEntity = restTemplate.exchange(
                baseURL + "/delete/" + id,
                HttpMethod.DELETE,
                new HttpEntity<>(null),
                new ParameterizedTypeReference<String>() {
                });
    }

    public Optional<Coach> getCoachById(String id) {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Coach> coachResponseEntity = null;

        try {
            coachResponseEntity = restTemplate.exchange(
                    baseURL + "/" + id,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<Coach>() {
                    });

            if (coachResponseEntity.getStatusCode() == HttpStatus.OK) {
                return Optional.of(coachResponseEntity.getBody());
            } else {
                return Optional.empty();
            }
        } catch (Exception e) {
            return Optional.empty();
        }
    }
    public List<Coach> getCoachByPlayingTeam(Country s) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Coach>> coachResponseEntity = restTemplate.exchange(
                baseURL +"/allbyplayingteam/"+ s,
                HttpMethod.GET,
                new HttpEntity<>(null),
                new ParameterizedTypeReference<List<Coach>>() {
                });
        return coachResponseEntity.getBody();
    }

    public void updateCoach(Coach coach) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Coach> coachHttpEntity = new HttpEntity<>(coach, null);
        String id = coach.getId();
        restTemplate.put(baseURL + "/update/" + id, coachHttpEntity, Coach.class);
    }

    public void saveCoach(Coach coach) {
        RestTemplate restTemplate = new RestTemplate();
        String id = coach.getId();
        HttpEntity<Coach> coachHttpEntity = new HttpEntity<>(coach, null);
        restTemplate.put(baseURL + "/save", coachHttpEntity, Coach.class);
    }


}
