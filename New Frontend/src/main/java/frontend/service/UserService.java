package frontend.service;

import frontend.model.Coach;
import frontend.model.Ground;
import frontend.model.Team;
import frontend.model.User;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public class UserService {
    private String baseURL = "http://localhost:8081/api/user";

    public Set<User> getAllUser() {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Set<User>> userResponseEntity = restTemplate.exchange(
                baseURL + "/all",
                HttpMethod.GET,
                new HttpEntity<>(null),
                new ParameterizedTypeReference<Set<User>>() {
                });

        return userResponseEntity.getBody();
    }

    public List<User> getUser(String name) {
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<List<User>> userResponseEntity = restTemplate.exchange(
                baseURL + "/all/" + name,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<User>>() {
                });

        return userResponseEntity.getBody();
    }

    public Optional<User> getUserById(String id) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<User> userResponseEntity = null;

        try {
            userResponseEntity = restTemplate.exchange(
                    baseURL + "/" + id,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<User>() {
                    });

            if (userResponseEntity.getStatusCode() == HttpStatus.OK) {
                return Optional.of(userResponseEntity.getBody());
            } else {
                return Optional.empty();
            }
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public void delete(User user) {
        RestTemplate restTemplate = new RestTemplate();
        String id = user.getId();
        ResponseEntity<String> userResponseEntity = restTemplate.exchange(
                baseURL + "/delete/" + id,
                HttpMethod.DELETE,
                new HttpEntity<>(null),
                new ParameterizedTypeReference<String>() {
                });

    }

    public Optional<User> saveUser(User user) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<User> userResponseEntity = null;

        userResponseEntity = restTemplate.exchange(
                baseURL + "/save",
                HttpMethod.POST,
                new HttpEntity<>(user),
                new ParameterizedTypeReference<User>() {
                });

        return Optional.of(userResponseEntity.getBody());

    }

    public void updateUser(User user) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<User> userHttpEntity = new HttpEntity<>(user, null);
        String id = user.getId();
        restTemplate.put(baseURL + "/update/" + id, userHttpEntity, User.class);
    }
}
