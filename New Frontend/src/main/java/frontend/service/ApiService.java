package frontend.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import frontend.model.home.Articles;
import frontend.model.home.Matches;
import frontend.model.home.MatchesScheduleModel;
import frontend.model.home.NewsPickerModel;

import java.util.List;

@Service
public class ApiService {
    public List<Articles> getAllTopHeadlines() {
        RestTemplate restTemplate = new RestTemplate();
        NewsPickerModel forObject = restTemplate.getForObject("https://newsapi.org/v2/top-headlines?sources=espn-cric-info&apiKey=434e3d43dc0249caa06b812a44739aaa", NewsPickerModel.class);
        return forObject.getArticles();
    }
    public List<Articles> getAllRecentHeadlines() {
        RestTemplate restTemplate = new RestTemplate();
        NewsPickerModel forObject = restTemplate.getForObject("https://newsapi.org/v2/everything?sources=espn-cric-info&apiKey=434e3d43dc0249caa06b812a44739aaa", NewsPickerModel.class);
        return forObject.getArticles();
    }
    public List<Matches> getAllNewMatches(){
        RestTemplate restTemplate = new RestTemplate();
        MatchesScheduleModel Object = restTemplate.getForObject("https://cricapi.com/api/matches?apikey=8roPV1IHd6glgHxIkFswOFQBCTC3", MatchesScheduleModel.class);
        return Object.getMatches();
    }
}
