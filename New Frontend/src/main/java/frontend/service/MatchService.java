package frontend.service;

import frontend.model.Match;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

public class MatchService {
    private String baseURL = "http://localhost:8081/api/match";

    public List<Match> getAllMatch() {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Match>> matchResponseEntity = restTemplate.exchange(
                baseURL + "/all",
                HttpMethod.GET,
                new HttpEntity<>(null),
                new ParameterizedTypeReference<List<Match>>() {
                });

        return matchResponseEntity.getBody();
    }


    public Optional<Match> getMatchById(String id) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Match> matchResponseEntity = null;

        try {
            matchResponseEntity = restTemplate.exchange(
                    baseURL + "/" + id,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<Match>() {
                    });

            if (matchResponseEntity.getStatusCode() == HttpStatus.OK) {
                return Optional.of(matchResponseEntity.getBody());
            } else {
                return Optional.empty();
            }
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public void delete(Match match) {
        RestTemplate restTemplate = new RestTemplate();
        String id = match.getMatchId();
        ResponseEntity<String> matchResponseEntity = restTemplate.exchange(
                baseURL + "/delete/" + id,
                HttpMethod.DELETE,
                new HttpEntity<>(null), String.class);

    }

    public Optional<Match> saveMatch(Match match) {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Match> matchResponseEntity = null;
        try {
            matchResponseEntity = restTemplate.exchange(
                    baseURL + "/save" ,
                    HttpMethod.POST,
                    new HttpEntity<>(match),
                    new ParameterizedTypeReference<Match>() {
                    });
            if (matchResponseEntity.getStatusCode() == HttpStatus.OK) {
                return Optional.of(matchResponseEntity.getBody());
            } else {
                return Optional.empty();
            }
        } catch (Exception e) {
            System.out.println(e);
            return Optional.empty();
        }

    }

    public void updateMatch(Match match) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Match> matchHttpEntity = new HttpEntity<>(match, null);
        String id = match.getMatchId();
        restTemplate.put(baseURL + "/update/" + id, matchHttpEntity, Match.class);
    }
}
