package frontend.service;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import frontend.model.Team;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public class TeamService {

    private String baseURL = "http://localhost:8081/api/team";

    public Set<Team> getAllTeam() {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Set<Team>> teamResponseEntity = restTemplate.exchange(
                baseURL + "/all",
                HttpMethod.GET,
                new HttpEntity<>(null),
                new ParameterizedTypeReference<Set<Team>>() {
                });

        return teamResponseEntity.getBody();
    }

    public List<Team> getTeam(String name) {
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<List<Team>> teamResponseEntity = restTemplate.exchange(
                baseURL + "/all/" + name,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Team>>() {
                });

        return teamResponseEntity.getBody();
    }

    public Optional<Team> getTeamById(String id) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Team> teamResponseEntity = null;

        try {
            teamResponseEntity = restTemplate.exchange(
                    baseURL + "/" + id,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<Team>() {
                    });

            if (teamResponseEntity.getStatusCode() == HttpStatus.OK) {
                return Optional.of(teamResponseEntity.getBody());
            } else {
                return Optional.empty();
            }
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public void delete(Team team) {
        RestTemplate restTemplate = new RestTemplate();
        String id = team.getTeamId().toString();
        ResponseEntity<String> teamResponseEntity = restTemplate.exchange(
                baseURL + "/delete/" + id,
                HttpMethod.DELETE,
                new HttpEntity<>(null),
                new ParameterizedTypeReference<String>() {
                });

    }

    public Optional<Team> saveTeam(Team team) {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Team> teamResponseEntity = null;

        teamResponseEntity = restTemplate.exchange(
                baseURL + "/save",
                HttpMethod.POST,
                new HttpEntity<>(team),
                new ParameterizedTypeReference<Team>() {
                });

        return Optional.of(teamResponseEntity.getBody());

    }

    public void updateTeam(Team team) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Team> teamHttpEntity = new HttpEntity<>(team, null);
        String id = team.getTeamId().toString();
        restTemplate.put(baseURL + "/update/" + id, teamHttpEntity, Team.class);
    }
}
