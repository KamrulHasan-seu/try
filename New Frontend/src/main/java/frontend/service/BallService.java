package frontend.service;

import frontend.model.BallDTO;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import frontend.model.Ball;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BallService {
    private String baseURL = "http://localhost:8081/api/ball";

    public List<Ball> getAllBall() {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Ball>> ballResponseEntity = restTemplate.exchange(
                baseURL + "/all",
                HttpMethod.GET,
                new HttpEntity<>(null),
                new ParameterizedTypeReference<List<Ball>>() {
                });

        return ballResponseEntity.getBody();
    }

    public Ball getBall(String name) {
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Ball> ballResponseEntity = restTemplate.exchange(
                baseURL + "/all/" + name,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Ball>() {
                });

        return ballResponseEntity.getBody();
    }

    public Optional<Ball> getBallById(String id) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Ball> ballResponseEntity = null;

        try {

            ballResponseEntity = restTemplate.exchange(
                    baseURL + "/" + id,
                    HttpMethod.GET,
                    new HttpEntity<>(null),
                    new ParameterizedTypeReference<Ball>() {
                    });

            if (ballResponseEntity.getStatusCode() == HttpStatus.OK) {
                return Optional.of(ballResponseEntity.getBody());
            } else {
                return Optional.empty();
            }
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public void delete(Ball ball) {
        RestTemplate restTemplate = new RestTemplate();
        String id = ball.getBallId();
        ResponseEntity<String> ballResponseEntity = restTemplate.exchange(
                baseURL + "/delete/" + id,
                HttpMethod.DELETE,
                new HttpEntity<>(null),
                new ParameterizedTypeReference<String>() {
                });

    }
    public Optional<BallDTO> saveBall(BallDTO ball) {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<BallDTO>  ballResponseEntity = restTemplate.exchange(
                baseURL + "/save",
                HttpMethod.POST,
                new HttpEntity<>(ball),
                new ParameterizedTypeReference<BallDTO>() {
                });

        return Optional.of(ballResponseEntity.getBody());

    }

    public void updateBall(BallDTO  ball) {
        RestTemplate restTemplate = new RestTemplate();
        System.out.println("ball:"+ball);
        HttpEntity<BallDTO> ballHttpEntity = new HttpEntity<>(ball, null);
        String id = ball.getBall().getBallId();
        System.out.println("ballId:" + id);
        restTemplate.put(baseURL + "/update/" + id, ballHttpEntity, Ball.class);
    }
    public List<Ball> getAllBymatchId(String id) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Ball>> ballResponseEntity = null;

        try {

            ballResponseEntity = restTemplate.exchange(
                    baseURL + "/getAllByMatchId/" + id,
                    HttpMethod.GET,
                    new HttpEntity<>(null),
                    new ParameterizedTypeReference<List<Ball>>() {
                    });

            System.out.println(ballResponseEntity.getBody());

            if (ballResponseEntity.getStatusCode() == HttpStatus.OK) {
                return ballResponseEntity.getBody();
            } else {
                return new ArrayList<>();
            }
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
}
