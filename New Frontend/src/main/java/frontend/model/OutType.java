package frontend.model;

public enum OutType {
    BOWLED,
    LEG_BEFORE_WICKET,
    RUN_OUT,
    STUMPED,
    CAUGHT
}
