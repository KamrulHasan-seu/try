package frontend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Team {
    private Country teamId;
    private Match match;
    private Set<Player> playerSet;

    private Set<Player> playingTeam;
    private Set<Player> substitutes;
    private Set<Coach> coachSet;


    public Team(Country teamId, Set<Player> playerList, Set<Coach> coach) {
        this.teamId = teamId;
        this.playerSet = playerList;
        this.coachSet = coach;
    }

    public Team(Country teamId, Match match, Set<Player> playingTeam, Set<Player> substitutes, Set<Coach> coachSet) {
        this.teamId = teamId;
        this.match = match;
        this.playingTeam = playingTeam;
        this.substitutes = substitutes;
        this.coachSet = coachSet;
    }

    public boolean isPersisted() {
        return teamId!=null;
    }


}
