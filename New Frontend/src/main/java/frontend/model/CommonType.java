package frontend.model;

public enum CommonType {

    DOT("Dot"),
    NOBALL("No Ball"),
    WIDE("Wide"),
    LEGBYE("Leg Bye"),
    BYE("Bye"),
    BOWLED("Bowled"),
    LEG_BEFORE_WICKET("LBW"),
    RUN_OUT("Run Out"),
    STUMPED("Stumped"),
    CAUGHT("Caught");

    private final String name;

    private CommonType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
