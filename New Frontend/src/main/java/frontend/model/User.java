package frontend.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Set;

@NoArgsConstructor
@Data
@EqualsAndHashCode
public class User extends Person {
    private String id;
    private String userName;
    private String email;
    private String password;
    private Set<Roles> roles;



}
