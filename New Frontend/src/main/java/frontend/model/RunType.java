package frontend.model;


public enum RunType {
    DOT,
    SINGLE,
    DOUBLE,
    THREE,
    FOUR,
    SIX
}