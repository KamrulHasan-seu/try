package frontend.model;

public enum PlayerRole {
    WICKETKEEPER,
    BATSMAN,
    BOWLER,
    SUBSTITUTE
}
