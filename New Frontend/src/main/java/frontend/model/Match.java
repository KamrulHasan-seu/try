package frontend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Match  {
    private String matchId;
    private MatchType matchType;
    private Innings innings;
    private Ground ground;
    private Team team1;
    private Team team2;
    private Team homeTeam;
    private LocalDate startMatchDate;
    private LocalDate endMatchDate;
    private LocalTime matchStartTime;
    private LocalTime matchEndsTime;
    private List<Ball> ballList;


    public Match(String matchId, MatchType matchType, Ground ground, Team team1, Team team2, Team homeTeam, LocalDate startMatchDate,
                 LocalDate endMatchDate, LocalTime matchStartTime, LocalTime matchEndsTime, List<Ball> ballList) {
        this.matchId = matchId;
        this.matchType = matchType;
        this.ground = ground;
        this.team1 = team1;
        this.team2 = team2;
        this.homeTeam = homeTeam;
        this.startMatchDate = startMatchDate;
        this.endMatchDate = endMatchDate;
        this.matchStartTime = matchStartTime;
        this.matchEndsTime = matchEndsTime;
        this.ballList = ballList;
    }
    public boolean isPersisted() {
        return matchId != null;
    }
}
