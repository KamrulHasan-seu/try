package frontend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;


@NoArgsConstructor
@Data
public class Comment {
    private User user;
    private LocalDateTime dateTime;
    private String message;
    private Comment responseToComment;
    private List<User> likedByList;
    private List<User> dislikedByList;

    public Comment(User user, LocalDateTime dateTime, String message, Comment responseToComment, List<User> likedByList,
                   List<User> dislikedByList) {
        this.user = user;
        this.dateTime = dateTime;
        this.message = message;
        this.responseToComment = responseToComment;
        this.likedByList = likedByList;
        this.dislikedByList = dislikedByList;
    }

    public Comment(User user, LocalDateTime dateTime, String message, List<User> likedByList, List<User> dislikedByList) {
        this.user = user;
        this.dateTime = dateTime;
        this.message = message;
        this.likedByList = likedByList;
        this.dislikedByList = dislikedByList;
    }
}
