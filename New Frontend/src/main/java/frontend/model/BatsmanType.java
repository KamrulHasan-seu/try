package frontend.model;

public enum BatsmanType {
    LEFT_HANDED,
    RIGHT_HANDED;
}
