package frontend.model.home;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class NewsPickerModel {
    private String status;
    private int totalResults;
    private List<Articles> articles;
}
