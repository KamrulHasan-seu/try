package frontend.model.home;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Matches {
    private int unique_id;
    private LocalDate date;
    private LocalDateTime dateTimeGMT;
    private String team1;
    private String team2;
    private String type;
    private boolean squad;
    private String toss_winner_team;
    private String winner_team;
    private boolean matchStarted;
}
