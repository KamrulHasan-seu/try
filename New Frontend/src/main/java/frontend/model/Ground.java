package frontend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class Ground {
    private String groundId;
    private String groundName;
    private String city;
    private String country;
    private double longitude;
    private double latitude;
    private int capacity;
    private LocalDate inaugurationDate;


    public boolean isPersisted() {
        return groundId != null;
    }

    public Ground(String groundId, String groundName, String city, String country, double longitude, double latitude,
                  int capacity, LocalDate inaugurationDate) {
        this.groundId = groundId;
        this.groundName = groundName;
        this.city = city;
        this.country = country;
        this.longitude = longitude;
        this.latitude = latitude;
        this.capacity = capacity;
        this.inaugurationDate = inaugurationDate;
    }
}
