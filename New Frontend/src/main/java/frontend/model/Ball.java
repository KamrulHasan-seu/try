package frontend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class Ball  {
    private String ballId;
    private Match matchId;
    private LocalDateTime dateTime;
    private int overNumber;
    private int ballNumber;
    private Player bowler;
    private Player batsman;
    private BallType ballType;
    private double ballSpeed;
    private String commentary;
    private List<Comment> commentList;

    public Ball(Match matchId, LocalDateTime dateTime, int overNumber, int ballNumber, Player bowler, Player batsman,
                BallType ballType, double ballSpeed, String commentary) {
        this.matchId = matchId;
        this.dateTime = dateTime;
        this.overNumber = overNumber;
        this.ballNumber = ballNumber;
        this.bowler = bowler;
        this.batsman = batsman;
        this.ballType = ballType;
        this.ballSpeed = ballSpeed;
        this.commentary = commentary;
    }

    public Ball(String ballId, Match matchId, LocalDateTime dateTime, int overNumber, int ballNumber, Player bowler, Player batsman, BallType ballType, double ballSpeed, String commentary) {
        this.ballId = ballId;
        this.matchId = matchId;
        this.dateTime = dateTime;
        this.overNumber = overNumber;
        this.ballNumber = ballNumber;
        this.bowler = bowler;
        this.batsman = batsman;
        this.ballType = ballType;
        this.ballSpeed = ballSpeed;
        this.commentary = commentary;
    }
}
