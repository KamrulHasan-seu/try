package frontend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BallDTO {

    private Ball ball;
    private String runType;
    private String outType;
    private Player outPlayer;
    private List<String> extraType;
    private Innings innings;

}