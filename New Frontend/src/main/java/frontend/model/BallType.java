package frontend.model;

public enum BallType {
    LEG_BREAK,
    GOOGLY,
    TOPSPIN,
    FLIPPER,
    SLIDER,
    FLICKER,
    OFF_BREAK,
    DOOSRA,
    ARM_BALL,
    CARROM_BALL,
    TEESRA,
    BOUNCER,
    IN_SWINGER,
    REVERSE_SWING,
    LEFT_CUTTER,
    OFF_CUTTER,
    OUT_SWINGER,
    YORKER,
    BEEMER,
    KNUCKLE_BALL,
    SLOWER_BALL

}
