package frontend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Player extends Person {
    private String playerId;
    private String playingTeam;
    private int jerseyNumber;
    private Set<PlayerRole> playerRole;
    private BatsmanType batsmanType;
    private BowlerType bowlerType;

    public Player(String firstName, String middleName, String lastName, String nationality, LocalDate dateOfBirth, double height,
                  String playerId, String playingTeam, int jerseyNumber, Set<PlayerRole> playerRole, BatsmanType batsmanType,
                  BowlerType bowlerType) {
        super(firstName, middleName, lastName, nationality, dateOfBirth, height);
        this.playerId = playerId;
        this.playingTeam = playingTeam;
        this.jerseyNumber = jerseyNumber;
        this.playerRole = playerRole;
        this.batsmanType = batsmanType;
        this.bowlerType = bowlerType;
    }

    public boolean isPersisted() {
        return playerId != null;
    }



}

