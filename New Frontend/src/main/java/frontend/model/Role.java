package frontend.model;

import lombok.Data;
import lombok.NoArgsConstructor;



@NoArgsConstructor
@Data
public class Role {
    private String id;
    private String Roles;

    public Role(String roles) {
        Roles = roles;
    }
}
