package frontend.model;

public enum Innings {

    FIRST_INNINGS("First Innings"), SECOND_INNINGS("Second Innings");

    private final String name;
    private Innings(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
