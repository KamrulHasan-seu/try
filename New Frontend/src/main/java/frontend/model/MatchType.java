package frontend.model;

public enum MatchType {
    TEST,
    ODI,
    T20,
    T10
}
