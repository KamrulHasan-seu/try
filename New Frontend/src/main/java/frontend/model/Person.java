package frontend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private String firstName;
    private String middleName;
    private String lastName;
    private String nationality;
    private LocalDate dateOfBirth;
    private double height;

}
