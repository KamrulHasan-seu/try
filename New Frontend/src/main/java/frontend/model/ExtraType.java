package frontend.model;

public enum ExtraType {
    NOBALL,
    WIDE,
    LEGBYE,
    BYE
}
