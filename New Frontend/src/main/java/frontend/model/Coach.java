package frontend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode
public class Coach extends Person {
    private String id;
    private Country nowCoachingTeam;
    private Set<Country> coachesFor;
    private LocalDate coachingStartsDate;
    private LocalDate coachingEndsDate;

    public boolean isPersisted() {
        return id != null;
    }

    public Coach(String firstName, String middleName, String lastName, String nationality, LocalDate dateOfBirth, double height,
                 String id, Country nowCoachingTeam, LocalDate coachingStartsDate, LocalDate coachingEndsDate) {
        super(firstName, middleName, lastName, nationality, dateOfBirth, height);
        this.id = id;
        this.nowCoachingTeam = nowCoachingTeam;
        this.coachingStartsDate = coachingStartsDate;
        this.coachingEndsDate = coachingEndsDate;
    }


    public Coach(Country nowCoachingTeam, LocalDate coachingEndsDate) {
        this.nowCoachingTeam = nowCoachingTeam;
        this.coachingEndsDate = coachingEndsDate;
    }

    public Coach(Country nowCoachingTeam, LocalDate coachingStartsDate, LocalDate coachingEndsDate) {
        this.nowCoachingTeam = nowCoachingTeam;
        this.coachingStartsDate = coachingStartsDate;
        this.coachingEndsDate = coachingEndsDate;
    }

}
